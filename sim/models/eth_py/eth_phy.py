#!/usr/bin/python

"""
A set of functions based on ethernet networks will be provided for
simulate that design. Ethernet network is based on the OSI model.
The layers contained in that class works with specific options
detailed in each function.
The layers offered in that simulation are:
- GMII (Gigabit Media Independent Interface).
- DLL  (Data Link Layer).
- IP   (Network Layer: Internet Protocol).
- UDP  (Transport Layer: User Datagram Protocol).
"""

# * import python libraries

import os
import cocotb
from cocotb.triggers import RisingEdge, FallingEdge, Lock
from cocotb.utils import get_sim_time
from cocotb.drivers import BusDriver
from cocotb.binary import BinaryValue
from dll import mac_encapsulate, mac_unencapsulate, arp_make, arp_check

import numpy as np

# * create class

class eth(BusDriver):
    """
    Ethernet Network protocol functions with GMII bus.
    """

    _signals = ["tx_data",    # data from IP to PHY
                "tx_valid",   # valid data from IP to PHY
                "rx_data",    # data from PHY to IP
                "rx_valid"]   # valid data from PHY to IP

    def __init__(self, entity, name, clock, **kwargs):
        """
        Initializing bus signals
        """

        BusDriver.__init__(self, entity, name, clock, **kwargs)

        self.bus.rx_data  <= 0
        self.bus.rx_valid <= 0

        self._lock = Lock()

        # call DEBUG mode
        if not os.environ['DEBUG']:
            self.DEBUG = False
        else:
            self.DEBUG = True
            import IPython

    async def _send(self,data):
        """
        Creates a low level interface between DLL and PHY.
        This communication is based on 803.2a for gigabit ethernet,
        but MII compatibility is (already) not supported.
        This function sends ethernet frames to HDL design.
        """

        # calculate total number of bytes to send to PHY
        data_length = len(data)


        # waiting to the next rising edge clock
        await RisingEdge(self.clock)

        # send all bytes with valid asserted
        for i in range(data_length):

            self.bus.rx_data  <= int(data[i])
            self.bus.rx_valid <= 1
            await RisingEdge(self.clock)

        # when data has been sent, deassert valid
        self.bus.rx_data  <= 0
        self.bus.rx_valid <= 0
        await RisingEdge(self.clock)


    async def _receive(self):
        """
        Creates a low level interface between DLL and PHY.
        This comunication is based on 802.3a for gigabit ethernet,
        but MII compatibility is (already) not supported.
        This function receives ethernet frames from HDL design.
        """

        max_frame = 1500
        data_rx = [0 for i in range(max_frame)]
        bindata = BinaryValue()
        i = 0

        await RisingEdge(self.bus.tx_valid)

        while self.bus.tx_valid == int(1):
            await RisingEdge(self.clock)
            bindata <= int(self.bus.tx_data)
            data_rx[i] = bindata.integer
            i += 1

        return data_rx[0:i-1]

    async def send(self,mac_src, ip_src, mac_dst, ip_dst, cmd):

        # MAC Broadcast
        mac_bcast = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]

        # compose info
        if self.DEBUG:
            mac_src_str  = ":".join(str(e) for e in mac_src)
            ip_src_str   = ".".join(str(e) for e in ip_src)
            mac_dst_str  = ":".join(str(e) for e in mac_dst)
            ip_dst_str   = ".".join(str(e) for e in ip_dst)
            type_msg_str = str(cmd)
            self.log.info("Compose message:\n -MAC Source: {}\n -IP Source: {}\n -MAC Destiny: {}\n -IP Destiny: {}\n -Message Type: {}" \
                          .format(mac_src_str,ip_src_str,mac_dst_str,ip_dst_str,type_msg_str))

        # compose and send ARP Request message
        if cmd.find('arp_rqst') != -1:
            type_msg = 0
            payload  = arp_make(mac_src, ip_src, mac_bcast, ip_dst, type_msg)
            frame    = mac_encapsulate(mac_src, mac_bcast, payload)
            await self._send(frame)
            if self.DEBUG:
                self.log.info("ARP Request message has been sent")

        # compose and send ARP Reply message
        if cmd.find('arp_rply') != -1:
            type_msg = 1
            payload  = arp_make(mac_src, ip_src, mac_dst, ip_dst, type_msg)
            frame    = mac_encapsulate(mac_src, mac_dst, payload)
            await self._send(frame)
            if self.DEBUG:
                self.log.info("ARP Reply message has been sent")

    async def receive(self,mac_src, ip_src, mac_dst, ip_dst, cmd):

        frame   = await self._receive()
        payload = mac_unencapsulate(self, mac_src, mac_dst, frame)

        if cmd.find('arp_rqst') != -1:
            type_msg = 0
            arp_check(self, mac_src, ip_src, mac_dst, ip_dst, type_msg, payload)
            if self.DEBUG:
                self.log.info("ARP Request Message has been checked")

        if cmd.find('arp_rply') != -1:
            type_msg = 1
            arp_check(self, mac_src, ip_src, mac_dst, ip_dst, type_msg, payload)
            if self.DEBUG:
                self.log.info("ARP Reply Message has been checked")
