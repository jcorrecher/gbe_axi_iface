#!/usr/bin/python

import numpy as np
from crc32 import crc32

def mac_encapsulate(mac_src,mac_dst,data):
    """
    Encapsulate frame data with SFD, MAC destiny, MAC source and FCS.
    TODO : If payload is smaller than 48bytes, fill data with padding.
    """

    # mac length in bytes
    mac_length   = 2 * 6
    # sfd length in bytes
    sfd_length   = 8
    # fcs length in bytes
    fcs_length   = 4
    # obtain data length
    frame_length = len(data)
    # calculate new length with the encapsulate length
    new_length   = sfd_length + mac_length + frame_length + fcs_length

    # create new frame vector
    v_frame  = [0 for i in range(new_length)]
    # initialize crc
    fcs = 0

    # initialize headers
    v_frame[0:7]   = np.repeat(0x55,7)
    v_frame[7]     = 0xD5
    v_frame[8:14]  = mac_dst
    v_frame[14:20] = mac_src

    # add data
    v_frame[20:-4] = data

    # calculate fcs
    (fcs,residue) = crc32(v_frame[8:-4])

    pos = new_length - fcs_length
    # add bytes at the end of the frame
    for i in range(fcs_length):
        # take LSB with a mask
        fcs_byte = fcs & 0x000000FF
        # save in the position frame
        v_frame[pos] = fcs_byte
        # shift fcs
        fcs = fcs >> 8
        # increase position
        pos += 1

    return v_frame

def mac_unencapsulate(self,mac_src, mac_dst, data):
    """
    Unencapsulate MAC fields, check FCS and return payload
    """

    # crc32 residue
    crc_residue   = [0xC7, 0x04, 0xDD, 0x7B]
    # mac length in bytes
    mac_length    = 2 * 6
    # sfd length in bytes
    sfd_length    = 8
    # fcs length in bytes
    fcs_length    = 4
    # obtain data length
    frame_length  = len(data)
    # calculate new length with the encapsulate
    new_length    = frame_length - (sfd_length + mac_length + fcs_length)
    # initialize residue in bytes
    residue_byte  = [0 for i in range(fcs_length)]
    #create new frame vector
    v_frame       = [0 for i in range(new_length)]
    # assign payload to v_frame
    v_frame       = data[20:-4]
    # calculate fcs
    (fcs,residue) = crc32(data[8:])

    # save and compare MACS
    rx_mac_src     = data[8:14]
    rx_mac_dst     = data[14:20]

    if rx_mac_src != mac_src:
        self.log.error("MAC source match failed,\n Expected MAC {}\n Received MAC {}" \
                       .format(mac_src, rx_mac_src))

    if rx_mac_dst != mac_dst:
        self.log.error("MAC destination match failed,\n Expected MAC {}\n Received MAC {}" \
                       .format(mac_dst, rx_mac_dst))

    # check calculated residue
    for i in range(fcs_length):
        # take LSB with a mask
        residue_byte[i] = residue & 0xFF
        # shift fcs
        residue = residue >> 8
        # bit swap
        residue_byte[i] = int(format(residue_byte[i], '0%db' % 8)[::-1],2)

    if (residue_byte != crc_residue):
        self.log.error("FCS Match Failed,\n Expected Residue {}\n Received Residue {}" \
                       .format(crc_residue, residue_byte))

    return v_frame


def arp_make(mac_src, ip_src, mac_dst, ip_dst, type_msg):
    """
    Creates ARP message. Two ARP type messages are implemented:
    * type_msg = 0 ARP Request. From a MAC&IP source, and IP destiny, obtains MAC destiny.
    * type_msg = 1 ARP Reply. Reply the MAC destiny desired.
    Payload:
    - Ethertype | Hardware type | IP Protocol version | Hardware Size |
    ARP Request/Reply | Sender MAC | Sender IP | Target MAC | Target IP | Padding
    """

    # types length in bytes
    type_length = 10 * 2
    # mac length in bytes
    mac_length  = 2 * 6
    # ip length in bytes
    ip_length   = 2 * 4
    # padding length for ARP message in bytes
    padding_arp_length = 18
    # Ethertype
    eth_type = [0x08, 0x06]
    # Hardware Type
    hw_type  = [0x00, 0x01]
    # IP Protocol
    ip_v4    = [0x08, 0x00]
    # Hardware Size
    hw_size  = [0x06, 0x04]
    # ARP Request
    arp_rqst = [0x00, 0x01]
    # ARP Reply
    arp_rply = [0x00, 0x02]


    # calculate new data length
    data_length = type_length + mac_length + ip_length + padding_arp_length
    # create frame vector
    v_frame = [0 for i in range(data_length)]

    # compose payload
    v_frame[0:2] = eth_type
    v_frame[2:4] = hw_type
    v_frame[4:6] = ip_v4
    v_frame[6:8] = hw_size
    if type_msg == 0:
        v_frame[8:10] = arp_rqst
    else:
        v_frame[8:10] = arp_rply
    v_frame[10:16] = mac_src
    v_frame[16:20] = ip_src
    v_frame[20:26] = mac_dst
    v_frame[26:30] = ip_dst

    return v_frame

def arp_check(self,mac_src, ip_src, mac_dst, ip_dst, type_msg, data):
    """
    Checks received ARP messages. Two ARP type messages are implemented:
    * type_msg = 0 ARP Request. From a MAC&IP source, and IP destiny, obtains MAC destiny.
    * type_msg = 1 ARP Reply. Reply the MAC destiny desired.
    Payload:
    - Ethertype | Hardware type | IP Protocol version | Hardware Size |
    ARP Request/Reply | Sender MAC | Sender IP | Target MAC | Target IP | Padding
    """

    # Ethertype
    eth_type  = [0x08, 0x06]
    # Hardware Type
    hw_type   = [0x00, 0x01]
    # IP Protocol
    ip_v4     = [0x08, 0x00]
    # Hardware Size
    hw_size   = [0x06, 0x04]
    # ARP Request
    arp_rqst  = [0x00, 0x01]
    # ARP Reply
    arp_rply  = [0x00, 0x02]
    # MAC Broadcast
    mac_bcast = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
    # obtain data length
    frame_length = len(data)

    if data[0:2] != eth_type:
        self.log.error("Received Ethertype {} differs from expected {}" \
                       .format(data[0:2], eth_type))

    if data[2:4] != hw_type:
        self.log.error("Received Hardware Type {} differs from expected {}" \
                       .format(data[2:4], hw_type))

    if data[4:6] != ip_v4:
        self.log.error("Received IP Version {} differs from expected {}" \
                       .format(data[4:6], ip_v4))

    if data[6:8] != hw_size:
        self.log.error("Received Hardware Size {} differs from expected {}" \
                       .format(data[6:8], hw_size))

    if type_msg == 0:
        if data[8:10] != arp_rqst:
            self.log.error("Received ARP Request Code {} differs from expected {}" \
                           .format(data[8:10], arp_rqst))
        if data[20:26] != mac_empty:
            self.log.error("Received MAC Empty {} differs from expected {}" \
                           .format(data[20:26], mac_emptyt))
    elif type_msg == 1:
        if data[8:10] != arp_rply:
            self.log.error("Received ARP Reply Code {} differs from expected {}" \
                           .format(data[8:10], arp_rply))
        if data[20:26] != mac_src:
            self.log.error("Received MAC Source Address {} differs from expected {}" \
                           .format(data[20:26], mac_src))

    if data[10:16] != mac_dst:
        self.log.error("Received MAC Destiny Address {} differs from expected {}" \
                           .format(data[10:16], mac_dst))

    if data[16:20] != ip_dst:
        self.log.error("Received IP Destiny Address {} differs from expected {}" \
                           .format(data[16:20], ip_dst))

    if data[26:30] != ip_src:
        self.log.error("Received IP Source Address {} differs from expected {}" \
                           .format(data[26:30], ip_src))
