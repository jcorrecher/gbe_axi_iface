#!/usr/bin/python

import numpy as np

def crc32(data):
    """
    Computes CRC-32 used in IEEE 802.3 ethernet frames for a byte vector.
    """

    # initialize values
    crc    = np.uint32(0xFFFFFFFF)
    adding = np.uint32(0x00000000)
    mask   = np.uint32(0x00000000)
    # polynomial function reversal reciprocal
    poly   = np.uint32(0xEDB88320)
    # poly   = np.uint32(0x82608EDB)


    # obtain length of data vector
    v_length = len(data)

    # convert data vector to uint8
    data_byte = np.uint8(data)
    for i in range(0, v_length):
        # calculate xor
        crc = crc ^ data_byte[i]
        #shift bit a bit
        for j in range(0,8):
            # calculate polynomial mask
            adding = crc & np.uint32(1)
            mask = np.uint32(~adding)
            if mask == (2**32)-1:
                mask = 0
            else:
                mask += 1
            # apply polynomial mask to shifted crc
            crc = (crc >> 1) ^ (poly & mask)

    # bitwise final
    crc = (np.uint32(~crc),np.uint32(crc))

    return crc
