#!/usr/bin/python

"""
This script collects a class to stablish a custom AXI4-Stream communication with command and status ports of HDL model of AXI-GBE interface.
- Send function will be used for command port
- Receiver function will be used for status port.
- Cmd function will compose the command to send.
- Check function will check status received.
This custom AXI4-Stream will contains ready, valid and data signals.

"""


# * import python libraries
import cocotb
from cocotb.triggers import RisingEdge, ReadOnly, ReadWrite, Lock
from cocotb.drivers import BusDriver
from cocotb.result import ReturnValue
from cocotb.binary import BinaryValue
import array

# * exception class
class AxiSendFail(Exception):
    """
    Custom exceptiuon signaling send transaction issues.
    """

    def __init__(self,message):
        self.message = message

# * axi4-stream control class
class axis_ctrl(BusDriver):
    """
    This class is compatible with AMBA 4 AXI4-Stream Protocol
    """

    _signals = ["ready", # slave can accept a transfer
                "valid", # master driving a valid transfer
                "data"]  # data send through the interface

    # ** initialize values
    def __init__(self, entity, name, clock, **kwargs):

        BusDriver.__init__(self, entity, name, clock **kwargs)

        # initialize values
        self.bus.valid <= 0
        self.bus.data  <= 0
        self.bus.ready <= 1

        # Mute for each channel that we master to prevent contention
        self._lock = Lock()

        self.log.debug("AXI4-Stream control bus created")

    # ** send function
    async def send(self, data):
        """
        Send at least one word through AXI4-Stream bus while slave is ready.
        """

        # waiting to the next rising edge clock
        await RisingEdge(self.clock)


        # waiting for asserted ready signal
        if self.bus.ready == 0:
            await RisingEdge(self.bus.ready)


        # drive data and assert valid
        self.bus.data  <= data
        self.bus.valid <= 1
        # move to the next cycle
        await RisingEdge(self.clock)

        # transfer finished, all the signals reset
        self.bus.data  <= 0
        self.bus.valid <= 0

        # ** receive function
    async def receive(self):
        """
        Receive at least one word from AXI4-stream. Ready must be asserted.
        """
        data = BinaryValue()
        # when valid asserts
        await RisingEdge(self.bus.valid)
        # capture data
        data <= int(self.bus.data)
        # move to the next rising edge
        await RisingEdge(self.clock)

        return data.integer
