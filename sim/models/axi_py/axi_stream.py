#!/usr/bin/python

"""
This file provides the functions to stablish a custom AXI4-Stream
communication with command and status ports.
Send will be used for command port, while receive will be
used for status port.
That custom AXI4-Stream will contains ready, valid and data.

"""

# * import python libraries
import os
import cocotb
from cocotb.triggers import RisingEdge, ReadOnly, ReadWrite, Lock
from cocotb.drivers import BusDriver
from cocotb.result import ReturnValue
from cocotb.binary import BinaryValue

import array

class AxiSendFail(Exception):
    """
    Custom exception signaling send transaction issues.
    """

    def __init__(self,message):
        self.message = message

class axi_stream(BusDriver):
    """
    This class is compatible with AMBA 4 AXI4-Stream Protocol
    """

    _signals = ["ready", # slave can accept a transfer
                "valid", # master driving valid transfer
                #"last",  # indicates the boundary of a packet
                # "keep",  # associated byte of valid data
                "data"]  # data pass across the interface

    def __init__(self, entity, name, clock, **kwargs):

        BusDriver.__init__(self, entity, name, clock, **kwargs)

        # self.bus.keep  <= 0
        # self.bus.last  <= 0

        if name.find('cmd') != -1:
            self.bus.valid <= 0
            self.bus.data  <= 0
            self.bus.ready <= 1
        else:
            self.bus.ready <= 1

        # Mutex for each channel that we master to prevent contention
        #self.send_data_busy = Lock("%s_TREADY" % name)
        self._lock = Lock()

        # call DEBUG mode
        if not os.environ['DEBUG']:
            self.DEBUG = False
        else:
            self.DEBUG = True
            import IPython

    async def send(self, data):
        """
        Send at least one word through AXI4-Stream bus while ready is asserted.
        """

        # waiting to the next rising edge clock
        await RisingEdge(self.clock)


        # waiting for asserted ready signal
        if self.bus.ready == 0:
            await RisingEdge(self.bus.ready)

        # drive data and assert valid
        self.bus.data  <= data
        self.bus.valid <= 1
        # move to the next cycle
        await RisingEdge(self.clock)

        # transfer finished, all the signals reset
        self.bus.data  <= 0
        self.bus.valid <= 0

    async def receive(self):
        """
        Receive at least one word from AXI4-stream. Ready must be asserted.
        """
        data = BinaryValue()
        # when valid asserts
        await RisingEdge(self.bus.valid)
        # capture data
        data <= int(self.bus.data)
        # move to the next rising edge
        await RisingEdge(self.clock)

        return data.integer

    async def cmd(self, mac, ip, udp, oper):
        """
        Send command : This function will send a command to the design through command
        AXI stream interface. The command contains:
        - Operation Code
        - MAC Address
        - IP Address
        - UDP Port
        """

        # initialize values
        cmd      = 0
        word_len = 13
        word     = [0 for i in range(word_len)]

        # check input sizes
        if len(mac) != 6:
            self.log.error("MAC address size differs to the value")
        if len(ip) != 4:
            self.log.error("IP address size differs to the value")
        if len(udp) != 2:
            self.log.error("UDP port size differs to the value")

      # compose info
        if self.DEBUG:
            mac_str  = ":".join(str(e) for e in mac)
            ip_str   = ".".join(str(e) for e in ip)
            udp_str  = "".join(str(e) for e in udp)
            oper_str = str(oper)
            self.log.info("Compose AXI Command:\n -Operation Mode: {}\n -MAC: {}\n -IP: {}\n -UDP: {}" \
                          .format(oper_str,mac_str,ip_str,udp_str))

        # compose command word
        word[0:2]  = udp
        word[2:6]  = ip
        word[6:12] = mac
        word[12]   = oper

        # create a sole command
        for i in range(word_len):
            cmd = cmd + (word[i] * 2**(i*8))

        # send command through axi command interface
        try:
            await self.send(cmd)
        except AxiSendFail as e:
            self.dut.log.error("command: %s" % (e))

    async def check(self,mac, ip, udp, oper):
        """
        Check Command: This function receives data from AXI command interface and
        compare these values with input ones.
        """

        data = await self.receive()

        bytes_per_word = 13
        # convert data in byte array
        bytes = [0 for i in range(bytes_per_word)]
        for i in range(bytes_per_word):
            bytes[i] = data & 0xFF;
            data = data >> 8

        # compare obtained fields with expected fields
        if bytes[0:2] != udp:
            if self.DEBUG:
                self.log.error("Received UDP port {} differs from expected {}" \
                               .format(bytes[0:2], udp))
        if bytes[2:6] != ip:
            if self.DEBUG:
                self.log.error("Received IP address {} differs from expected {}" \
                               .format(bytes[2:6], ip))
        if bytes[6:12] != mac:
            if self.DEBUG:
                self.log.error("Received MAC address {} differs from expected {}" \
                               .format(bytes[6:12], mac))
        if bytes[12] != oper:
            if self.DEBUG:
                self.log.error("Received Operation {} differs to expected {}" \
                               .format(bytes[12], oper))

        if self.DEBUG:
            self.log.info("AXI Received fields are as expected")
