#+TITLE: ARP Protocol Simulation with Cocotb and GHDL

* Context

This folder contains a simulation to evaluate Address Resolution Protocol.


* Simulation behavior

A previous configuration parameters are setted in the core by master interface.
ARP Request message is received by ethernet port. Message is analyzed and
reported by status interface. Data received are analyzed by Master. ARP reply
command is setted for core by master. ARP reply message is received and
checked by Ethernet endpoint.

The following picture shows simulation timeline:

#+html: <p align="center"><img src="../../../resources/images/arp_timeline.png" /></p>

* Test hierarchy

This simulation uses the following files:
  - [[https://gitlab.com/jcorrecher/gbe_axi_iface/-/blob/master/sim/runs/gbe_axi_arp.cocotb/hdl/gbe_axi_iface_tb.vhd][hdl/gbe_axi_iface_tb.vhd]] : Vhdl testbench
  - [[https://gitlab.com/jcorrecher/gbe_axi_iface/-/blob/master/sim/runs/gbe_axi_arp.cocotb/gbe_axi_iface_tb.py][gbe_axi_iface_tb.py]] : Python testbench
  - [[https://gitlab.com/jcorrecher/gbe_axi_iface/-/tree/master/sim/models][../../models]] : Python models
  - [[https://gitlab.com/jcorrecher/gbe_axi_iface/-/blob/master/sim/runs/gbe_axi_arp.cocotb/Makefile][Makefile]] : Cocotb Simulation makefile


* TXT Stimulus

none

* Simulation tool

This simulation runs in linux OS with coroutine based cosimulation testbench and
ghdl simulator tool. Only VHDL files can be used with this simulation tool.


** run the simulation
#+begin_src sh
  make
#+end_src

** show timing diagram with gtkwave tool
#+begin_src sh
  make gtkwave
#+end_src

** clear the simulation
#+begin_src sh
  make clear
#+end_src
