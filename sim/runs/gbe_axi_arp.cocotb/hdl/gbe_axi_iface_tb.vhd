-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : gbe_axi_iface_tb.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-04-27
-- Last update: 2021-01-28
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This module is a top wrapper for cocotb simulation
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-04-27  1.0      jocorso Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.record_declaration.all;

-------------------------------------------------------------------------------
-- * entity wrapper
-------------------------------------------------------------------------------

entity gbe_axi_iface_tb is

  port (
    --!\name gbe interface
    --!\{
    rx_clk         : in  std_logic;
    phy_rx_valid   : in  std_logic;
    phy_rx_data    : in  unsigned(7 downto 0);
    tx_clk         : out std_logic;
    phy_tx_valid   : out std_logic;
    phy_tx_data    : out unsigned(7 downto 0);
    --!\}
    --!\name axi command interface
    --!\{
    axi_clk        : in  std_logic;
    axi_rst        : in  std_logic;
    axi_cmd_data   : in  unsigned(103 downto 0);
    axi_cmd_valid  : in  std_logic;
    axi_cmd_ready  : out std_logic;
    axi_stat_data  : out unsigned(103 downto 0);
    axi_stat_valid : out std_logic;
    axi_stat_ready : in  std_logic
   --!\}
    );

end entity gbe_axi_iface_tb;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture tsb of gbe_axi_iface_tb is

-- * signal declaration

  signal b1_gbe_phy_rc : rc_gbe_phy_bus(id_phy(phy_rx_data'range),
                                        od_phy(phy_tx_data'range));
  signal b1_axi_rc     : rc_axi_bus(ctrl(cmd(data(103 downto 0)),
                                         stat(data(103 downto 0))));

begin

-------------------------------------------------------------------------------
-- * B0 :  port <-> signal assignment
-------------------------------------------------------------------------------

  -- ** inputs
  b1_gbe_phy_rc.rx_clk     <= rx_clk;
  b1_gbe_phy_rc.ic_valid   <= phy_rx_valid;
  b1_gbe_phy_rc.id_phy     <= phy_rx_data;
  --
  b1_axi_rc.ctrl.aclk       <= axi_clk;
  b1_axi_rc.ctrl.arst       <= axi_rst;
  b1_axi_rc.ctrl.cmd.data   <= axi_cmd_data;
  b1_axi_rc.ctrl.cmd.valid  <= axi_cmd_valid;
  b1_axi_rc.ctrl.stat.ready <= axi_stat_ready;

  -- ** outputs
  tx_clk       <= b1_gbe_phy_rc.rx_clk;
  phy_tx_valid <= b1_gbe_phy_rc.oc_valid;
  phy_tx_data  <= b1_gbe_phy_rc.od_phy;

  axi_cmd_ready  <= b1_axi_rc.ctrl.cmd.ready;
  axi_stat_valid <= b1_axi_rc.ctrl.stat.valid;
  axi_stat_data  <= b1_axi_rc.ctrl.stat.data;

-------------------------------------------------------------------------------
-- * B1 : top instantiation
-------------------------------------------------------------------------------

  u_gbe_axi_iface : entity work.gbe_axi_iface
    port map (
      phy => b1_gbe_phy_rc,
      axi => b1_axi_rc);


end architecture tsb;
