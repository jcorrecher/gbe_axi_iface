#!/usr/bin/python

'''
This script will provide a complete simulation for gigabit ethernet - AXI interface core.
'''

# * Import libraries and modules

import sys,os
import cocotb
import logging
import binascii
from cocotb.triggers import Timer, FallingEdge, RisingEdge, with_timeout
from cocotb.clock import Clock
from cocotb.utils import get_sim_time
from cocotb import fork
from cocotb.result import SimTimeoutError, TestSuccess
from random import uniform
import numpy as np

# ** import python model functions

# TODO: move to $PYTHONPATH
sys.path.append(os.path.abspath(os.path.join('..','..','models','axi_py')))
from axi_stream import axi_stream
sys.path.append(os.path.abspath(os.path.join('..','..','models','eth_py')))
from eth_phy import eth

#from ..models.eth_phy import eth_phy
#from ..models.axi_stream import axi_stream

# * Setup variables

# Reset length in us
RESET_LENGTH = 2

# Random time between input events
MIN_INTERVAL = 50
MAX_INTERVAL = 1000

# Number of input triggers
NUM_EVENTS = int(os.environ['NUM_EVENTS'])

# Random length frame
MIN_FRAME = 47
MAX_FRAME = 1500

# MAC Empty
MAC_EMPTY =[0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
# IP Empty
IP_EMPTY  =[0x00, 0x00, 0x00, 0x00]
# UDP Empty
UDP_EMPTY =[0x00, 0x00]

# MAC Device
MAC_DEV =[0x00, 0x55, 0xDB, 0x44, 0xFF, 0xEE]
# IP Device : 172.30.163.26
IP_DEV = [0xAC, 0x1E, 0xA3, 0x1A]
# UDP Device
UDP_DEV = [0x12,0x20]

# MAC Core
MAC_CORE = [0x35, 0x55, 0xDB, 0x44, 0xFF, 0xEE]
# IP Core : 172.30.163.122
IP_CORE = [0xAC, 0x1E, 0xA3, 0x7A]
# UDP Source
UDP_CORE = [0x11, 0x20]

# Commands
CMD0 = 0x00
CMD1 = 0x01
CMD2 = 0x02

# timeout watchdog
TIMEOUT = int(os.environ['TIMEOUT'])


# * 3.- Testbench class

class testbench(object):
    """
    Testbench class. Implements infrastructure for testing dut.
    """

    # ** init define

    def __init__(self,dut):
        """
        Initialize dut entity
        """

        # call DEBUG mode
        if not os.environ['DEBUG']:
            self.DEBUG = False
        else:
            self.DEBUG = True
            import IPython

        # assign dut
        self.dut = dut

        # init default inputs

        # ethernet inputs
        dut.phy_rx_valid <= 0
        dut.phy_rx_data  <= 0
        # axi-stream inputs
        dut.axi_cmd_valid <= 0
        dut.axi_cmd_data  <= 0

        # create a fork for gbe clock input
        fork(Clock(dut.rx_clk, 8, units='ns').start())
        # create a fork for axi clock
        fork(Clock(dut.axi_clk, 20, units='ns').start())

        # creating axi command bus
        self.axicmd = axi_stream(dut, "axi_cmd", dut.axi_clk)

        if self.DEBUG:
            self.dut._log.info("AXI command bus created")

        # creating axi command bus
        self.axistat = axi_stream(dut, "axi_stat", dut.axi_clk)

        if self.DEBUG:
            self.dut._log.info("AXI status bus created")

        # creating eth_phy bus
        self.gmii = eth(dut, "phy", dut.rx_clk)

        if self.DEBUG:
            self.dut._log.info("GMII bus created")

        # initialize reset
        fork(self.reset(RESET_LENGTH))


    # ** reset

    async def reset(self,duration):
        """
        Initial Reset for AXI interface
        """

        self.dut.axi_rst <= 1
        await Timer(duration, units="us")
        self.dut.axi_rst <= 0
        self.dut.axi_rst._log.debug("Reset Complete")


    # ** ARP test routine

    async def arp_rqst(self):
        """
        This routine unleash a number of steps from an external
        device in order to obtain MAC destination address.
        """

        # Core configuration parameters
        await self.axicmd.cmd(MAC_CORE,IP_CORE,UDP_CORE,0)

        # 1. generates an ARP request from an external device.
        await self.gmii.send(MAC_DEV, IP_DEV, MAC_EMPTY, IP_CORE, 'arp_rqst')
        # 2. status word received by master notes ARP query is OK.
        await self.axistat.check(MAC_DEV,IP_DEV,UDP_EMPTY,1)
        # 3. send command word to generate ARP reply.
        await self.axicmd.cmd(MAC_DEV,IP_DEV,UDP_EMPTY,2)
        # 4. status word received by master notes ARP reply sent.
        fork(self.axistat.check(MAC_DEV,IP_DEV,UDP_EMPTY,2))
        # 5. external device receives the reply message OK.
        await self.gmii.receive(MAC_CORE, IP_CORE, MAC_DEV, IP_DEV, 'arp_rply')


# * 4.- Main test

@cocotb.test()
async def fd_test1(dut):
    """
    Create triggers from 10ns clock source to 12ns clock destiny.
    Calling driver and monitor.
    """

    dut._log.info("Start running test.\tTime {:2.1f} ns ({:2.1f} us)".
                  format(get_sim_time('ns'), get_sim_time('us')))

    # load testbench
    tb = testbench(dut)

    # wait to reset release
    await Timer(2*RESET_LENGTH, units='us')

    # send configuration parameters
    fork(tb.arp_rqst())

    await Timer(10, units='us')

    raise TestSuccess
