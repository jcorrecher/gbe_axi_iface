-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : ctrl_cmd.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-05-25
-- Last update: 2020-06-08
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This module will manage AXI4-Stream command interface.
--              configuration parameters will come across that interface in
--              order to configure all the parameters of Tx and Rx messages.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-05-25  1.0      jocorso Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.record_declaration.all;
use work.constant_declaration.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity ctrl_cmd is

  port (
    --!\name control bus
    --!\{
    ctrl : inout rc_ctrl_bus
   --!\}
    );

end entity ctrl_cmd;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture rtl of ctrl_cmd is

-- ** alias declaration
-- *** global
  alias clk             : std_logic is ctrl.clk;
  alias arst            : std_logic is ctrl.arst;
-- *** axi
  alias id_cmd          : unsigned is ctrl.conf.axi.data;
  alias ic_valid        : std_logic is ctrl.conf.axi.valid;
  alias oc_ready        : std_logic is ctrl.conf.axi.ready;
-- *** arp
  alias od_arp_mac_dst  : unsigned is ctrl.conf.dll.arp.mac_dst;
  alias od_arp_mac_src  : unsigned is ctrl.conf.dll.arp.mac_src;
  alias od_arp_ip_dst   : unsigned is ctrl.conf.dll.arp.ip_dst;
  alias od_arp_ip_src   : unsigned is ctrl.conf.dll.arp.ip_src;
  alias oc_arp_tx_start : std_logic is ctrl.conf.dll.arp.start;
  alias oc_arp_tx_conf  : unsigned is ctrl.conf.dll.arp.msg_type;
  alias ic_arp_tx_done  : std_logic is ctrl.conf.dll.arp.done;

-- ** signal declaration
  signal bc_srl_ra        : unsigned(0 to 1)                   := (others => '0');
  signal bc_rst_s         : std_logic                          := '0';
  signal b0_valid_s       : std_logic                          := '0';
  --! TODO : operation mode must be common, not only ARP
  signal b0_op_s          : unsigned(oc_arp_tx_conf'range)     := (others => '0');
  --signal b0_udp_s         : unsigned(od_arp_udp_dst'range)     := (others => '0');
  signal b0_ip_s          : unsigned(od_arp_ip_dst'range)      := (others => '0');
  signal b0_mac_s         : unsigned(od_arp_mac_dst'range)     := (others => '0');
  --
--  signal b1_udp_src_r     : unsigned(od_arp_udp_src'range)     := (others => '0');
  signal b1_ip_src_r      : unsigned(od_arp_ip_src'range)      := (others => '0');
  signal b1_mac_src_r     : unsigned(od_arp_mac_src'range)     := (others => '0');
  --signal b1_udp_dst_r     : unsigned(od_arp_udp_dst'range)     := (others => '0');
  signal b1_ip_dst_r      : unsigned(od_arp_ip_dst'range)      := (others => '0');
  signal b1_mac_dst_r     : unsigned(od_arp_mac_dst'range)     := (others => '0');
  signal b1_done_s        : std_logic                          := '0';
  signal b1_done_r        : std_logic                          := '0';
  --
  signal b2_busy_r        : std_logic                          := '0';
  signal b2_valid_s       : std_logic                          := '0';
  signal b2_valid_r       : std_logic                          := '0';
  signal b2_eoc_s         : std_logic                          := '0';
  signal b2_cmd_r         : unsigned(oc_arp_tx_conf'range)     := (others => '0');
  signal b2_start_s       : std_logic                          := '0';
  signal b2_start_r       : std_logic                          := '0';
  --
  signal b3_arp_tx_done_s : std_logic                          := '0';

begin

-------------------------------------------------------------------------------
-- * BC : reset synchronizer
-------------------------------------------------------------------------------
--! This block synchronize an asynchronous reset from axi interface to
--! configuration domain

  process (clk) is
  begin
    if rising_edge(clk) then
      if (arst = '1') then
        bc_srl_ra <= (others => '1');
      else
        bc_srl_ra <= '0' & bc_srl_ra(0);
      end if;
    end if;
  end process;

  bc_rst_s <= bc_srl_ra(1);

-------------------------------------------------------------------------------
-- * B0 : command word split
-------------------------------------------------------------------------------
--! The received command will be divided in different subwords:
--! [0:15]   => UDP port
--! [32:47]  => IP address
--! [48:95]  => MAC address
--! [96:103] => Command

  --b0_udp_s <= id_cmd(C_CMD_UDP_MSB downto C_CMD_UDP_LSB);
  b0_ip_s  <= id_cmd(C_CMD_IP_MSB downto C_CMD_IP_LSB);
  b0_mac_s <= id_cmd(C_CMD_MAC_MSB downto C_CMD_MAC_LSB);
  b0_op_s  <= id_cmd(C_CMD_MSB downto C_CMD_LSB);

  b0_valid_s <= ic_valid;

  --! ready signal is deasserted mean while any command is in execution
  oc_ready <= not b2_busy_r and not b1_done_r;

-------------------------------------------------------------------------------
-- * B1 : Cmd 0 = Load source parameters
-------------------------------------------------------------------------------
--! By default source and destiny parameteres are empty. The master must load
--! depending the command set.
--! Command 0 is used to load source parameters (this design is the source).
--! Otherwise the parameters received are used for destiny parameters.
--! These parameters are : source UDP port, IP address and MAC address.

  process (clk) is
  begin
    if rising_edge(clk) then
      if (bc_rst_s = '1') then
        --b1_udp_dst_r <= (others => '0');
        b1_ip_dst_r  <= (others => '0');
        b1_mac_dst_r <= (others => '0');
        --b1_udp_src_r <= (others => '0');
        b1_ip_src_r  <= (others => '0');
        b1_mac_src_r <= (others => '0');
      else
        if (ic_valid = '1') then
          if (b0_op_s = to_unsigned(C_CMD_LOAD, b0_op_s'length)) then
            --  b1_udp_dst_r <= b1_udp_dst_r;
            b1_ip_dst_r  <= b1_ip_dst_r;
            b1_mac_dst_r <= b1_mac_dst_r;
            -- b1_udp_src_r <= b0_udp_s;
            b1_ip_src_r  <= b0_ip_s;
            b1_mac_src_r <= b0_mac_s;
          else
            --  b1_udp_dst_r <= b0_udp_s;
            b1_ip_dst_r  <= b0_ip_s;
            b1_mac_dst_r <= b0_mac_s;
            -- b1_udp_src_r <= b1_udp_src_r;
            b1_ip_src_r  <= b1_ip_src_r;
            b1_mac_src_r <= b1_mac_src_r;
          end if;
        end if;
      end if;
      b1_done_r <= b1_done_s;
    end if;
  end process;

  --! this done control signal is deasserted during loading parameters.
  b1_done_s <= '1' when b0_valid_s = '1' and b0_op_s = C_CMD_LOAD else '0';

-------------------------------------------------------------------------------
-- * B2 : cmd 1 & 2 = Send ARP message
-------------------------------------------------------------------------------
--! Command 1 & 2 are used to set a transmission of ARP message.
--! Command 1: ARP Request
--! Command 2: ARP Reply
--! The parameters loaded earlier are necessary.

  process (clk) is
  begin
    if rising_edge(clk) then
      --! when command word is read, busy control asserts in order to don't
      --! accept more commands.
      if (b2_eoc_s = '1') then
        b2_busy_r <= '0';
      else
        if (b2_valid_s = '1') then
          b2_busy_r <= '1';
        end if;
      end if;
      --! split command word
      if (b2_valid_s = '1') then
        b2_cmd_r <= b0_op_s;
      end if;
      --! shift register
      b2_valid_r <= b2_valid_s;
      b2_start_r <= b2_start_s;
    end if;
  end process;

  --! create start trigger with rising edge signals
  b2_start_s <= b2_valid_s and not b2_valid_r;

--! end of command execution
  b2_eoc_s <= b3_arp_tx_done_s;

  --! valid asserted when cmd 1 y 2 are detected
  b2_valid_s <= '1' when b0_valid_s = '1' and
                (b0_op_s = C_CMD_ARP_RQST or b0_op_s = C_CMD_ARP_RPLY)
                else '0';

-------------------------------------------------------------------------------
-- * B3 : assign signals <-> ports
-------------------------------------------------------------------------------

-- ** ARP
  od_arp_mac_dst  <= b1_mac_dst_r;
  od_arp_mac_src  <= b1_mac_src_r;
  od_arp_ip_dst   <= b1_ip_dst_r;
  od_arp_ip_src   <= b1_ip_src_r;
  oc_arp_tx_start <= b2_start_r;
  oc_arp_tx_conf  <= b2_cmd_r;

  b3_arp_tx_done_s <= ic_arp_tx_done;

end architecture rtl;
