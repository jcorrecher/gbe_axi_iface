-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : ctrl_stat.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-06-02
-- Last update: 2020-06-08
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This module reports to the master the data processed in that
--              core.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-06-02  1.0      jocorso Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.record_declaration.all;
use work.constant_declaration.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity ctrl_stat is

  port (
    --!\name control bus
    --!\{
    ctrl : inout rc_ctrl_bus
   --!\}
    );

end entity ctrl_stat;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture rtl of ctrl_stat is

-- ** alias declaration
-- *** global
  alias clk            : std_logic is ctrl.clk;
  alias arst           : std_logic is ctrl.arst;
-- *** axi
  alias od_stat        : unsigned is ctrl.stat.axi.data;
  alias oc_valid       : std_logic is ctrl.stat.axi.valid;
  alias ic_ready       : std_logic is ctrl.stat.axi.ready;
-- *** arp
  alias id_arp_mac_rx  : unsigned is ctrl.stat.dll.arp.rx_mac;
  alias id_arp_ip_rx   : unsigned is ctrl.stat.dll.arp.rx_ip;
  alias ic_arp_rx_done : std_logic is ctrl.stat.dll.arp.rx_done;
  alias ic_arp_rx_conf : unsigned is ctrl.stat.dll.arp.rx_conf;
  alias id_arp_mac_tx  : unsigned is ctrl.stat.dll.arp.tx_mac;
  alias id_arp_ip_tx   : unsigned is ctrl.stat.dll.arp.tx_ip;
  alias ic_arp_tx_done : std_logic is ctrl.stat.dll.arp.tx_done;
  alias ic_arp_tx_conf : unsigned is ctrl.stat.dll.arp.tx_conf;

-- ** signal declaration
  signal bc_srl_ra  : unsigned(0 to 1)                    := (others => '0');
  signal bc_rst_s   : std_logic                           := '0';
  --
  signal b0_valid_s : std_logic                           := '0';
  signal b0_valid_r : std_logic                           := '0';
  signal b0_stat_r  : unsigned(C_BYTE_W-1 downto 0)       := (others => '0');
  signal b0_ip_r    : unsigned(id_arp_ip_rx'range)        := (others => '0');
  signal b0_mac_r   : unsigned(id_arp_mac_rx'range)       := (others => '0');
  --
  signal b1_stat_s  : unsigned(ic_arp_tx_conf'range)      := (others => '0');
  signal b1_ip_s    : unsigned(id_arp_ip_rx'range)        := (others => '0');
  signal b1_mac_s   : unsigned(id_arp_mac_rx'range)       := (others => '0');

begin

-------------------------------------------------------------------------------
-- * BC : reset synchronizer
-------------------------------------------------------------------------------
--! This block synchronize an asynchronous reset from axi interface to
--! configuration domain

  process (clk) is
  begin
    if rising_edge(clk) then
      if (arst = '1') then
        bc_srl_ra <= (others => '1');
      else
        bc_srl_ra <= '0' & bc_srl_ra(0);
      end if;
    end if;
  end process;

  bc_rst_s <= bc_srl_ra(1);

-------------------------------------------------------------------------------
-- * B0 : merge status word
-------------------------------------------------------------------------------
--! Status word will be composed by data from the IP core and will be divided
--! in different subwords:
--! [0:15]   => UDP port
--! [32:47]  => IP address
--! [48:95]  => MAC address
--! [96:103] => Status

  od_stat(C_STAT_UDP_MSB downto C_STAT_UDP_LSB) <= (others => '0');
  od_stat(C_STAT_IP_MSB downto C_STAT_IP_LSB)   <= b0_ip_r;
  od_stat(C_STAT_MAC_MSB downto C_STAT_MAC_LSB) <= b0_mac_r;
  od_stat(C_STAT_MSB downto C_STAT_LSB)         <= b0_stat_r;

  oc_valid <= b0_valid_r;

  --! when one of the messages is done, status message is sent
  b0_valid_s <= ic_arp_tx_done or ic_arp_rx_done;

  process (clk) is
  begin
    if rising_edge(clk) then
      if (b0_valid_s = '1') then
        b0_stat_r <= b1_stat_s;
        b0_mac_r  <= b1_mac_s;
        b0_ip_r   <= b1_ip_s;
      end if;
      b0_valid_r <= b0_valid_s;
    end if;
  end process;

-------------------------------------------------------------------------------
-- * B1 : Status word
------------------------------------------------------------------------------
--! status word contains structured bits that indicates the follow functions:
--! 0 => ARP Request message has been sent
--! 1 => ARP Request message has been received
--! 2 => ARP Reply message has been sent
--! 3 => ARP Reply message has been received

  b1_stat_s <= x"00" when ic_arp_tx_done = '1' and ic_arp_tx_conf = x"01" else
               x"01" when ic_arp_rx_done = '1' and ic_arp_rx_conf = x"01" else
               x"02" when ic_arp_tx_done = '1' and ic_arp_tx_conf = x"02" else
               x"03" when ic_arp_rx_done = '1' and ic_arp_rx_conf = x"02" else
               x"FF";

  b1_ip_s <= id_arp_ip_rx when ic_arp_rx_done = '1' else
             id_arp_ip_tx when ic_arp_tx_done = '1' else
             C_IP_EMPTY;
  b1_mac_s <= id_arp_mac_rx when ic_arp_rx_done = '1' else
              id_arp_mac_tx when ic_arp_tx_done = '1' else
              C_MAC_EMPTY;

end architecture rtl;
