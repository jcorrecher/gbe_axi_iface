-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : axi_iface.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-06-04
-- Last update: 2020-06-05
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This module establish three AXI interfaces to handle the design
--              and provide the data from / trhough the IP Core and Master.
--              1. AXI stream with payload to send or receive.
--              2. AXI stream to set Transmission.
--              3. AXI stream to get status about TX and RX.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-06-04  1.0      jocorso Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.record_declaration.all;
use work.constant_declaration.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity axi_iface is

  port (
    --!\name AXI bus
    --!\{
    axi  : inout rc_axi_bus;
    --!\}
    --!\name control bus
    --!\{
    ctrl : inout rc_ctrl_bus
   --!\}
    );

end entity axi_iface;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture structural of axi_iface is

-- ** constant declaration
  constant C_DEPTH : natural := 8;

-- ** alias declaration
  alias ctrl_clk        : std_logic is ctrl.clk;
-- *** configuration alias
  alias m_axi_rx_aclk   : std_logic is axi.ctrl.aclk;
  alias m_axi_rx_arst   : std_logic is axi.ctrl.arst;
  alias m_axi_rx_tdata  : unsigned is axi.ctrl.cmd.data;
  alias m_axi_rx_tvalid : std_logic is axi.ctrl.cmd.valid;
  alias m_axi_rx_tready : std_logic is axi.ctrl.cmd.ready;
  alias oc_conf_data    : unsigned is ctrl.conf.axi.data;
  alias oc_conf_valid   : std_logic is ctrl.conf.axi.valid;
  alias ic_conf_ready   : std_logic is ctrl.conf.axi.ready;
-- *** status alias
  alias s_axi_tx_aclk   : std_logic is axi.ctrl.aclk;
  alias s_axi_tx_arst   : std_logic is axi.ctrl.arst;
  alias s_axi_tx_tdata  : unsigned is axi.ctrl.stat.data;
  alias s_axi_tx_tvalid : std_logic is axi.ctrl.stat.valid;
  alias s_axi_tx_tready : std_logic is axi.ctrl.stat.ready;
  alias ic_stat_data    : unsigned is ctrl.stat.axi.data;
  alias ic_stat_valid   : std_logic is ctrl.stat.axi.valid;
  alias oc_stat_ready   : std_logic is ctrl.stat.axi.ready;

begin

  ctrl.arst <= axi.ctrl.arst;

-------------------------------------------------------------------------------
-- * B0 : Data
-------------------------------------------------------------------------------


-------------------------------------------------------------------------------
-- * B1 : Configuration
-------------------------------------------------------------------------------
--! This is the AXI4-Stream command interface. It will act as Slave and
--! receives commands from Master.

  u_fifo_conf : entity work.fifo_axi_stream
    generic map (
      G_DEPTH => C_DEPTH)
    port map (
      m_axi_rx_aclk   => m_axi_rx_aclk,
      m_axi_rx_arst   => m_axi_rx_arst,
      m_axi_rx_tdata  => m_axi_rx_tdata,
      m_axi_rx_tvalid => m_axi_rx_tvalid,
      m_axi_rx_tready => m_axi_rx_tready,
      s_axi_tx_aclk   => ctrl_clk,
      s_axi_tx_tdata  => oc_conf_data,
      s_axi_tx_tvalid => oc_conf_valid,
      s_axi_tx_tready => ic_conf_ready);

-------------------------------------------------------------------------------
-- * B2 : Status
-------------------------------------------------------------------------------
--! This is the AXI4-Stream status interface. It will act as Master and send
--! the status info when processes are done.

  u_fifo_status : entity work.fifo_axi_stream
    generic map (
      G_DEPTH => C_DEPTH)
    port map (
      m_axi_rx_aclk   => ctrl_clk,
      m_axi_rx_arst   => s_axi_tx_arst,
      m_axi_rx_tdata  => ic_stat_data,
      m_axi_rx_tvalid => ic_stat_valid,
      m_axi_rx_tready => oc_stat_ready,
      s_axi_tx_aclk   => s_axi_tx_aclk,
      s_axi_tx_tdata  => s_axi_tx_tdata,
      s_axi_tx_tvalid => s_axi_tx_tvalid,
      s_axi_tx_tready => s_axi_tx_tready);

end architecture structural;
