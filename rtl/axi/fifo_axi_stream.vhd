-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : fifo_axi_stream.vhd
-- Author     : Jose Correcher  <jocorso@jocor-virtualbox>
-- Company    :
-- Created    : 2020-05-25
-- Last update: 2020-09-04
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This module creates a FIFO wrap to handle AXI4-Stream signals
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-05-25  1.0      jocorso	Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity fifo_axi_stream is

  generic (
    G_DEPTH : natural := 32);
  port (
    m_axi_rx_aclk   : in  std_logic;
    m_axi_rx_arst   : in  std_logic;
    m_axi_rx_tdata  : in  unsigned;
    m_axi_rx_tvalid : in  std_logic;
    m_axi_rx_tready : out std_logic;
    s_axi_tx_aclk   : in  std_logic;
    s_axi_tx_tdata  : out unsigned;
    s_axi_tx_tvalid : out std_logic;
    s_axi_tx_tready : in  std_logic);

end entity fifo_axi_stream;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------
architecture rtl of fifo_axi_stream is

-- ** signal declaration
  signal b0_empty_s : std_logic := '0';
  signal b0_full_s  : std_logic := '0';
  signal b1_ena_s   : std_logic := '0';

begin

-------------------------------------------------------------------------------
-- * B0 : FIFO instance
-------------------------------------------------------------------------------

  u_fifo : entity work.fifo
    generic map (
      G_DEPTH_L2 => G_DEPTH)
    port map (
      rst      => m_axi_rx_arst,
      wclk     => m_axi_rx_aclk,
      ic_wena  => m_axi_rx_tvalid,
      oc_full  => b0_full_s,
      id_write => m_axi_rx_tdata,
      rclk     => s_axi_tx_aclk,
      ic_rena  => b1_ena_s,
      oc_valid => s_axi_tx_tvalid,
      oc_empty => b0_empty_s,
      od_read  => s_axi_tx_tdata);

-------------------------------------------------------------------------------
-- * B1 : read control signals
-------------------------------------------------------------------------------

  b1_ena_s <= '1' when b0_empty_s = '0' and s_axi_tx_tready = '1' else '0';
  m_axi_rx_tready <= not b0_full_s and not m_axi_rx_arst;

end architecture rtl;
