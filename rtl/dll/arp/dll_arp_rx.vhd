-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : dll_arp_rx.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-05-18
-- Last update: 2021-03-17
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This module implements ARP receiving message. The purpose of
--              ARP message is to obtain the link layer address, such as MAC
--              address associated with a given internet layer address (IPv4)
--              defined by RFC826.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-05-18  1.0      jocorso Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.record_declaration.all;
use work.constant_declaration.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity dll_arp_rx is

  port (
    --!\name ethernet signals
    --!\{
    eth  : inout rc_eth_bus;
    --!\}
    --!\name control bus
    --!\{
    ctrl : inout rc_ctrl_bus
   --!\}
    );

end entity dll_arp_rx;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture rtl of dll_arp_rx is

  -- ** alias declaration
  -- *** gbe rx
  alias clk        : std_logic is eth.mii.rx_clk;
  alias id_phy     : unsigned  is eth.mii.rx_data;
  alias ic_valid   : std_logic is eth.mii.rx_valid;
  -- *** mac dst

  alias id_mac_dst : unsigned is ctrl.conf.dll.arp.mac_src;
  alias id_ip_dst  : unsigned is ctrl.conf.dll.arp.ip_src;
  alias od_mac_src : unsigned is ctrl.stat.dll.arp.rx_mac;
  alias od_ip_src  : unsigned is ctrl.stat.dll.arp.rx_ip;
  alias oc_conf    : unsigned is ctrl.stat.dll.arp.rx_conf;
  alias oc_done    : std_logic is ctrl.stat.dll.arp.rx_done;

-- ** signal declaration
  signal bc_srl_ra        : unsigned(0 to C_IP1_END)       := (others => '0');
  signal bc_data_r        : unsigned(id_phy'range)         := (others => '0');
  signal bc_eof_r         : std_logic                      := '0';
  --
  signal b0_sfd_r         : unsigned(C_SFD_W-1 downto 0)   := (others => '0');
  signal b0_mask_s        : std_logic                      := '0';
  --
  signal b1_mac_r         : unsigned(2*C_MAC_W-1 downto 0) := (others => '0');
  signal b1_mask_s        : std_logic                      := '0';
  --
  signal b2_ip_r          : unsigned(2*C_IP_W-1 downto 0)  := (others => '0');
  signal b2_eth_r         : unsigned(C_2BYTE_W-1 downto 0) := (others => '0');
  signal b2_arp_r         : unsigned(C_2BYTE_W-1 downto 0) := (others => '0');
  signal b2_ip_dst_mask_s : std_logic                      := '0';
  signal b2_ip_src_mask_s : std_logic                      := '0';
  signal b2_ip_mask_s     : std_logic                      := '0';
  signal b2_eth_mask_s    : std_logic                      := '0';
  signal b2_arp_mask_s    : std_logic                      := '0';
  --
  signal b3_data_r        : unsigned(id_phy'range)         := (others => '0');
  signal b3_mask_r        : std_logic                      := '0';
  signal b3_soc_s         : std_logic                      := '0';
  signal b3_eof_s         : std_logic                      := '0';
  signal b3_crc_s         : unsigned(id_phy'range)         := (others => '0');
  signal b3_crc_residue_s : unsigned(C_CRC_RESIDUE'range)  := (others => '0');
  signal b3_rdy_s         : std_logic                      := '0';
  --
  signal b4_ok_s          : std_logic                      := '0';
  signal b4_flags_s       : unsigned(4 downto 0)           := (others => '0');


-- ** alias internal declaration
  alias a_mac_dst : unsigned is b1_mac_r(b1_mac_r'length-C_MAC_W-1 downto 0);
  alias a_ip_dst  : unsigned is b2_ip_r(b2_ip_r'length-1 downto b2_ip_r'length-C_IP_W);

begin

-------------------------------------------------------------------------------
-- ** BC : srl valid and data register
-------------------------------------------------------------------------------
--! a valid shift register will be created. This srl will be used to create
-- load signal masks

  process (clk) is
  begin
    if rising_edge(clk) then
      bc_srl_ra <= ic_valid & bc_srl_ra(0 to bc_srl_ra'length-2);
      --! input data register to synchronize with delayed mask
      bc_data_r <= id_phy;
      --! valid falling edge
      bc_eof_r  <= bc_srl_ra(0) and not ic_valid;
    end if;
  end process;

-------------------------------------------------------------------------------
-- ** B0 : Start Frame Delimiting
-------------------------------------------------------------------------------
--! This block will load the first 8 bytes corresponding to SFD:
--! 0x55 0x55 0x55 0x55 0x55 0x55 0x55 0xD5
--! A SFD enable mask will be created to filt SFD and check.

  process (clk) is
  begin
    if rising_edge(clk) then
      --! b0_sfd_r stores SFD until msg finish.
      if (bc_eof_r = '1') then
        b0_sfd_r <= (others => '0');
      else
        if (b0_mask_s = '1') then
          b0_sfd_r <= b0_sfd_r(b0_sfd_r'length-id_phy'length-1 downto 0) & bc_data_r;
        end if;
      end if;
    end if;
  end process;

  --! Creating SFD enable mask (8bytes)
  b0_mask_s <= bc_srl_ra(C_SFD_INI) and not bc_srl_ra(C_SFD_END);

-------------------------------------------------------------------------------
-- ** B1 : MAC
-------------------------------------------------------------------------------
--! This block load MAC source and destiny.
--! A MAC enable mask will be created to load MACs received in the current
--! message.

  process (clk) is
  begin
    if rising_edge(clk) then
      --! b1_mac_r stores MACs until msg finish.
      if (bc_eof_r = '1') then
        b1_mac_r <= (others => '0');
      else
        if (b1_mask_s = '1') then
          b1_mac_r <= bc_data_r & b1_mac_r(b1_mac_r'length-1 downto id_phy'length);
        end if;
      end if;
    end if;
  end process;

--! Creating MAC enable mask (2x6bytes)
  b1_mask_s <= bc_srl_ra(C_MAC_INI) and not bc_srl_ra(C_MAC_END);

-------------------------------------------------------------------------------
-- ** B2 : Ethertype & IP
-------------------------------------------------------------------------------

  process (clk) is
  begin
    if rising_edge(clk) then
      --! stores data until msg finish
      if (bc_eof_r = '1') then
        b2_ip_r  <= (others => '0');
        b2_eth_r <= (others => '0');
        b2_arp_r <= (others => '0');
      else
        --! b2_ip_r stores IP address
        if (b2_ip_mask_s = '1') then
          b2_ip_r <= bc_data_r & b2_ip_r(b2_ip_r'length-1 downto id_phy'length);
        end if;
        --! b2_eth_r stores Ethertype
        if (b2_eth_mask_s = '1') then
          b2_eth_r <= bc_data_r & b2_eth_r(b2_eth_r'length-1 downto id_phy'length);
        end if;
        --! b2_arp_r stores arp flag
        if (b2_arp_mask_s = '1') then
          b2_arp_r <= bc_data_r & b2_arp_r(b2_arp_r'length-1 downto id_phy'length);
        end if;
      end if;
    end if;
  end process;

--! Creating IP Mask
  b2_ip_dst_mask_s <= bc_srl_ra(C_IP0_INI) and not(bc_srl_ra(C_IP0_END));
  b2_ip_src_mask_s <= bc_srl_ra(C_IP1_INI) and not(bc_srl_ra(C_IP1_END));
  b2_ip_mask_s     <= b2_ip_dst_mask_s or b2_ip_src_mask_s;
--! Creating Ethertype Mask
  b2_eth_mask_s    <= bc_srl_ra(C_ETH_INI) and not(bc_srl_ra(C_ETH_END));
-- Create ARP Request Mask
  b2_arp_mask_s    <= bc_srl_ra(C_ARP_INI) and not(bc_srl_ra(C_ARP_END));

-------------------------------------------------------------------------------
-- ** B3 : CRC
-------------------------------------------------------------------------------
--! This block will calculate FCS (CRC-32) residue. The calculation contains
--! MAC header + Ethertype + Payload + CRC.
--! A CRC enable mask will be created to enable CRC operation.
--! The result of the operation must be CRC-32 residue to catalog the frame
--! as valid.

  process (clk) is
  begin
    if rising_edge(clk) then
      --! when the frame ends, CRC enable deasserts
      if (b3_eof_s = '1') then
        b3_mask_r <= '0';
      else
        --! when SFD ends, CRC calculation mask is asserts
        if (b3_soc_s = '1') then
          b3_mask_r <= '1';
        end if;
      end if;
      --! shift register for input data
      b3_data_r <= id_phy;
    end if;
  end process;

  --! Start of CRC calculation
  b3_soc_s <= bc_srl_ra(C_SFD_END-1) and not bc_srl_ra(C_SFD_END);
  --! End of ethernet frame
  b3_eof_s <= bc_srl_ra(0) and not ic_valid;

  -- *** crc-32 instance
  u_rx_fcs : entity work.crc_32_ethernet
    port map (
      id     => b3_data_r,
      od     => b3_crc_s,
      od_crc => b3_crc_residue_s,
      ena    => b3_mask_r,
      rdy    => b3_rdy_s,
      clk    => clk);

-------------------------------------------------------------------------------
-- ** B4 : Check RX
-------------------------------------------------------------------------------
--! This block checks all the earlier data loaded to generates ARP answer and
--!  saves IP and MAC address to ARP table

--! check SFD -> 1: Ok, 0: Fail
  b4_flags_s(0) <= '1' when b0_sfd_r = C_PREAMBLE else '0';
--! check MAC destiny -> 1: Ok, 0: Fail
  b4_flags_s(1) <= '1' when ((a_mac_dst = id_mac_dst and b2_arp_r = C_ARP_RPLY) or
                             (a_mac_dst = C_MAC_BROADCAST and b2_arp_r = C_ARP_RQST)) else '0';
--! check IP destiny -> 1: Ok, 0: Fail
  b4_flags_s(2) <= '1' when a_ip_dst = id_ip_dst             else '0';
--! check ethertype -> 1: Ok, 0: Fail
  b4_flags_s(3) <= '1' when b2_eth_r = C_ETHERTYPE           else '0';
--! check FCS residue -> 1: Ok, 0: Fail
  b4_flags_s(4) <= '1' when b3_crc_residue_s = C_CRC_RESIDUE else '0';

--! all flags ok -> start answer and save MAC and IP src
  b4_ok_s <= and_reduce(std_logic_vector(b4_flags_s));

-------------------------------------------------------------------------------
-- * assign internal signals to ports
-------------------------------------------------------------------------------

    od_mac_src <= b1_mac_r(b1_mac_r'length-1 downto b1_mac_r'length-C_MAC_W);
    od_ip_src  <= b2_ip_r(od_ip_src'range);
    oc_done    <= b4_ok_s;
    oc_conf    <= b2_arp_r(b2_arp_r'length-1 downto oc_conf'length);

end architecture rtl;
