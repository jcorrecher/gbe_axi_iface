-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : dll_arp_tx.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-05-25
-- Last update: 2021-03-17
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This module implements ARP transmitting message.
--              That transmission can be an ARP reply or request depending on
--              configuration parameter. The purpose of ARP message is obtain
--              the link layer address, such as MAC address associated with a
--              given internet layer addres (IPv4) defined by RFC826.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-05-25  1.0      jocorso Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.record_declaration.all;
use work.constant_declaration.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity dll_arp_tx is

  port (
    --!\name ethernet signals
    --!\{
    eth  : inout rc_eth_bus;
    --!\}
    --!\name control bus
    --!\{
    ctrl : inout rc_ctrl_bus
   --!\}
    );

end entity dll_arp_tx;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture rtl of dll_arp_tx is

-- ** constant declaration
  constant C_CNT_W : natural := 32;

-- ** alias declaration
-- *** gbe
  alias clk          : std_logic is eth.mii.rx_clk;
  alias od_phy       : unsigned is eth.mii.tx_data;
  alias oc_valid     : std_logic is eth.mii.tx_valid;
-- *** conf
  alias id_mac_dst   : unsigned is ctrl.conf.dll.arp.mac_dst;
  alias id_ip_dst    : unsigned is ctrl.conf.dll.arp.ip_dst;
  alias id_mac_src   : unsigned is ctrl.conf.dll.arp.mac_src;
  alias id_ip_src    : unsigned is ctrl.conf.dll.arp.ip_src;
  alias ic_start     : std_logic is ctrl.conf.dll.arp.start;
  alias ic_tx_conf   : unsigned is ctrl.conf.dll.arp.msg_type;
  alias oc_conf_done : std_logic is ctrl.conf.dll.arp.done;
-- *** status
  alias od_mac       : unsigned is ctrl.stat.dll.arp.tx_mac;
  alias od_ip        : unsigned is ctrl.stat.dll.arp.tx_ip;
  alias oc_tx_conf   : unsigned is ctrl.stat.dll.arp.tx_conf;
  alias oc_stat_done : std_logic is ctrl.stat.dll.arp.tx_done;

-- ** signal declaration
  signal b0_valid_r   : std_logic                        := '0';
  signal b0_eotx_s    : std_logic                        := '0';
  signal b0_cnt_r     : unsigned(C_CNT_W-1 downto 0)     := (others => '0');
  --
  signal b1_rply_s    : unsigned(C_ARP_MSG_W-1 downto 0) := (others => '0');
  signal b1_rqst_s    : unsigned(C_ARP_MSG_W-1 downto 0) := (others => '0');
  signal b1_data_sa   : unsigned(C_ARP_MSG_W-1 downto 0) := (others => '0');
  signal b1_data_ra   : unsigned(C_ARP_MSG_W-1 downto 0) := (others => '0');
  signal b1_srl_ra    : unsigned(0 to C_SFD_END)         := (others => '0');
  signal b1_soc_s     : std_logic                        := '0';
  signal b1_crc_ena_r : std_logic                        := '0';
  signal b1_crc_rdy_s : std_logic                        := '0';
  signal b1_crc_rdy_r : std_logic                        := '0';
  signal b1_eotx_s    : std_logic                        := '0';
  signal b1_crc_s     : unsigned(od_phy'range)           := (others => '0');

begin

-------------------------------------------------------------------------------
-- * B0 : create tx valid
-------------------------------------------------------------------------------
--! This block will generate a control tx valid signal to send ARP message.
--! Valid signal is generated until ARP message length will be reached.

  process (clk) is
  begin
    if rising_edge(clk) then
      --! deassert valid when frame finish
      if (b0_eotx_s = '1') then
        b0_valid_r <= '0';
      else
        --! when command has been configured, enables the request msg
        if (ic_start = '1') then
          b0_valid_r <= '1';
        end if;
      end if;
      --! Tx message has a fixed number of sent words
      if (b0_valid_r = '0') then
        b0_cnt_r <= (others => '0');
      else
        b0_cnt_r <= b0_cnt_r + 1;
      end if;
    end if;
  end process;

  --! when length has been reached finish the transmission
  b0_eotx_s <= '1' when b0_cnt_r > C_ARP_MSG_LEN else '0';

-------------------------------------------------------------------------------
-- * B1 : compose tx message
-------------------------------------------------------------------------------
--! This block will compose the ARP message. The message content will be
--! modified depending on configuration parameter. Two type of ARP message has
--! been configured: Request and Reply. FCS must be calculated to join to the end
--! of the message. MAC destiny must be set to broadcast MAC when ARP request
--! message is configured.
--! The message is composed by the next fields:
--!  - Start Delimiting Frame | MAC Source | MAC Destiny | Ethertype
--!  - Hardware type | IP Protocol version | Hardware Size | ARP Request/Reply
--!  - MAC Destiny | IP Destiny | MAC Source | IP Source | Padding
--!  - FCS

  --! ARP reply message
  b1_rply_s <=
    C_PADDING &
    id_ip_src & id_mac_src & id_ip_dst & id_mac_dst &
    C_ARP_RPLY & C_HW_SIZE & C_IP_V4 & C_HW_TYPE &
    C_ETHERTYPE & id_mac_dst & id_mac_src & C_PREAMBLE;

  --! ARP request message
  b1_rqst_s <=
    C_PREAMBLE & C_MAC_BROADCAST & id_mac_src & C_ETHERTYPE &
    C_HW_TYPE & C_IP_V4 & C_HW_SIZE & C_ARP_RQST &
    C_MAC_EMPTY & id_ip_dst & id_mac_src & id_ip_src &
    C_PADDING;

  b1_rqst_s <=
    C_PADDING &
    id_ip_src & id_mac_src & id_ip_dst & C_MAC_EMPTY &
    C_ARP_RQST & C_HW_SIZE & C_IP_V4 & C_HW_TYPE &
    C_ETHERTYPE & id_mac_src & C_MAC_BROADCAST & C_PREAMBLE;

  --! message selector based on configuration command
  b1_data_sa <= b1_rply_s when ic_tx_conf = C_ARP_RPLY(C_2BYTE_W-1 downto C_BYTE_W) else b1_rqst_s;

  process (clk) is
  begin
    if rising_edge(clk) then
      --! load all fields except FCS
      if (ic_start = '1') then
        b1_data_ra <= b1_data_sa;
      else
        --! send byte each cycle
        if (b0_valid_r = '1') then
          b1_data_ra <= x"00" & b1_data_ra(b1_data_ra'length-1 downto od_phy'length);
        end if;
      end if;
      --! data valid delay to exclude SFD and creates enable CRC
      b1_srl_ra <= b0_valid_r & b1_srl_ra(0 to b1_srl_ra'length-2);
      --! create CRC enable to calculate FCS (excluding SFD)
      if (b0_eotx_s = '1') then
        b1_crc_ena_r <= '0';
      else
        if (b1_soc_s = '1') then
          b1_crc_ena_r <= '1';
        end if;
      end if;
      -- shift register
      b1_crc_rdy_r <= b1_crc_rdy_s;
    end if;
  end process;

  --! start of calculus: in that rising edge starts FCS calculate
  b1_soc_s <= b1_srl_ra(b1_srl_ra'high-2) and not b1_srl_ra(b1_srl_ra'high-1);

  --! asserts when message has been sent
  b1_eotx_s <= not b1_crc_rdy_s and b1_crc_rdy_r;

-- *** crc-32 instance
  --! FCS message must be calculated (in this module) and added to the message
  u_tx_fcs : entity work.crc_32_ethernet
    port map (
      id     => b1_data_ra(od_phy'length-1 downto 0),
      od     => b1_crc_s,
      od_crc => open,
      ena    => b1_crc_ena_r,
      rdy    => b1_crc_rdy_s,
      clk    => clk);

-------------------------------------------------------------------------------
-- * assign internal signals to ports
-------------------------------------------------------------------------------

  -- ** gbe
  od_phy <= b1_crc_s when b1_crc_rdy_s = '1' else
            b1_data_ra(od_phy'length-1 downto 0);
  oc_valid <= b0_valid_r or b1_crc_rdy_s;

  -- ** conf
  oc_stat_done <= b1_eotx_s;
  oc_conf_done <= b1_eotx_s;
  od_mac       <= id_mac_dst;
  od_ip        <= id_ip_dst;
  oc_tx_conf   <= ic_tx_conf;

end architecture rtl;
