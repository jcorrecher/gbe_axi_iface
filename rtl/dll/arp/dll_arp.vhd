-- * header
-------------------------------------------------------------------------------
-- Title      : GbE - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : dll_arp.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-05-18
-- Last update: 2020-06-05
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This module implements ARP protocol reply used for discovering
--              the link layer address, such as MAC address associated with a
--              given internet layer address (IPv4) defined by RFC826.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-05-18  1.0      jocorso	Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.record_declaration.all;
use work.constant_declaration.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity dll_arp is

  port (
    --!\name ethernet signals
    --!\{
    eth  : inout rc_eth_bus;
    --!\}
    --!\name control bus
    --!\{
    ctrl : inout rc_ctrl_bus
   --!\}
    );

end entity dll_arp;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture structural of dll_arp is

begin

-------------------------------------------------------------------------------
-- * B0 : receiver
-------------------------------------------------------------------------------

  u_dll_arp_rx : entity work.dll_arp_rx
    port map (
      eth  => eth,
      ctrl => ctrl);

-------------------------------------------------------------------------------
-- * B1 : transceiver
-------------------------------------------------------------------------------

  u_dll_arp_tx : entity work.dll_arp_tx
    port map (
      eth  => eth,
      ctrl => ctrl);

end architecture structural;
