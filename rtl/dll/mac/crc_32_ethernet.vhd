-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : crc_32_ethernet.vhd
-- Author     : Jose Correcher  <jocorso@jocor-virtualbox>
-- Company    :
-- Created    : 2020-04-28
-- Last update: 2020-05-15
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This module calculates CRC-32 for ethernet frames by using
--              Polynomial equation : x^32 + x^26 + x^23 + x^22 + x^16 + x^12 +
--              x^11 + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x + 1
--              The result is inverted and bit reversal order.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-04-28  1.0      jocorso Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity crc_32_ethernet is
  port(
    id     : in  unsigned (7 downto 0);
    od     : out unsigned (7 downto 0);
    od_crc : out unsigned (31 downto 0);
    ena    : in  std_logic;
    rdy    : out std_logic;
    clk    : in  std_logic);
end crc_32_ethernet;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture rtl of crc_32_ethernet is

  -- ** function declaration

  -- polynomial: (0 1 2 4 5 7 8 10 11 12 16 22 23 26 32)
  -- data width: 8
  -- convention: the first serial bit is D[7]

  function nextCRC32_D8(Data : unsigned(7 downto 0);
                        crc  : unsigned(31 downto 0))
    return unsigned is
    variable d      : unsigned(7 downto 0);
    variable c      : unsigned(31 downto 0);
    variable newcrc : unsigned(31 downto 0);
  begin
    d          := Data;
    c          := crc;
    newcrc(0)  := c(30) xor d(1) xor c(24) xor d(7);
    newcrc(1)  := d(6) xor d(7) xor d(0) xor c(30) xor c(31) xor d(1) xor c(24) xor c(25);
    newcrc(2)  := c(26) xor d(5) xor d(6) xor d(7) xor c(30) xor d(0) xor d(1) xor c(31) xor c(24) xor c(25);
    newcrc(3)  := d(4) xor c(26) xor d(5) xor c(27) xor d(6) xor d(0) xor c(31) xor c(25);
    newcrc(4)  := d(4) xor c(26) xor d(5) xor c(27) xor c(28) xor d(7) xor c(30) xor d(1) xor c(24) xor d(3);
    newcrc(5)  := d(4) xor c(27) xor d(6) xor c(28) xor d(7) xor c(29) xor c(30) xor d(0) xor d(1) xor c(31) xor d(2) xor c(24) xor d(3) xor c(25);
    newcrc(6)  := c(26) xor d(5) xor d(6) xor c(28) xor c(29) xor d(0) xor c(30) xor c(31) xor d(1) xor d(2) xor d(3) xor c(25);
    newcrc(7)  := d(4) xor c(26) xor d(5) xor c(27) xor d(7) xor c(29) xor d(0) xor c(31) xor d(2) xor c(24);
    newcrc(8)  := d(4) xor c(27) xor d(6) xor c(28) xor d(7) xor c(24) xor c(0) xor d(3) xor c(25);
    newcrc(9)  := c(26) xor d(5) xor d(6) xor c(28) xor c(29) xor d(2) xor d(3) xor c(25) xor c(1);
    newcrc(10) := d(4) xor c(26) xor c(2) xor d(5) xor c(27) xor d(7) xor c(29) xor d(2) xor c(24);
    newcrc(11) := d(4) xor c(27) xor d(6) xor c(3) xor c(28) xor d(7) xor c(24) xor d(3) xor c(25);
    newcrc(12) := c(26) xor d(5) xor d(6) xor c(28) xor d(7) xor c(4) xor c(29) xor c(30) xor d(1) xor d(2) xor c(24) xor d(3) xor c(25);
    newcrc(13) := d(4) xor c(26) xor d(5) xor c(27) xor d(6) xor c(29) xor d(0) xor c(30) xor c(5) xor c(31) xor d(1) xor d(2) xor c(25);
    newcrc(14) := d(4) xor c(26) xor d(5) xor c(27) xor c(28) xor c(30) xor d(0) xor d(1) xor c(31) xor c(6) xor d(3);
    newcrc(15) := d(4) xor c(27) xor c(28) xor c(29) xor d(0) xor c(31) xor d(2) xor c(7) xor d(3);
    newcrc(16) := c(28) xor d(7) xor c(29) xor d(2) xor c(24) xor d(3) xor c(8);
    newcrc(17) := c(9) xor d(6) xor c(29) xor c(30) xor d(1) xor d(2) xor c(25);
    newcrc(18) := c(26) xor d(5) xor c(10) xor c(30) xor d(0) xor d(1) xor c(31);
    newcrc(19) := d(4) xor c(27) xor c(11) xor d(0) xor c(31);
    newcrc(20) := c(28) xor c(12) xor d(3);
    newcrc(21) := c(29) xor c(13) xor d(2);
    newcrc(22) := d(7) xor c(14) xor c(24);
    newcrc(23) := d(6) xor d(7) xor c(30) xor d(1) xor c(15) xor c(24) xor c(25);
    newcrc(24) := c(26) xor d(5) xor d(6) xor d(0) xor c(31) xor c(16) xor c(25);
    newcrc(25) := d(4) xor c(17) xor c(26) xor d(5) xor c(27);
    newcrc(26) := d(4) xor c(18) xor c(27) xor c(28) xor d(7) xor c(30) xor d(1) xor c(24) xor d(3);
    newcrc(27) := d(6) xor c(19) xor c(28) xor c(29) xor d(0) xor c(31) xor d(2) xor d(3) xor c(25);
    newcrc(28) := c(26) xor d(5) xor c(20) xor c(29) xor c(30) xor d(1) xor d(2);
    newcrc(29) := d(4) xor c(27) xor c(21) xor c(30) xor d(0) xor d(1) xor c(31);
    newcrc(30) := c(28) xor d(0) xor c(22) xor c(31) xor d(3);
    newcrc(31) := c(29) xor c(23) xor d(2);
    return newcrc;
  end nextCRC32_D8;

  -- ** constant declaration
  constant higha : unsigned (31 downto 0) := x"FFFFFFFF";

  -- ** signal declaration
  signal lc_rdy_ra : unsigned(0 to 2)      := (others => '0');
  signal lc_ena_r  : std_logic             := '0';
  signal ld_crc_r  : unsigned(31 downto 0) := (others => '0');
  signal lc_mux_s  : unsigned(0 to 1);
  signal lc_dv_s   : std_logic;
  signal lc_rdy_s  : std_logic;
  signal ld_crc_s  : unsigned(31 downto 0);

begin

  -----------------------------------------------------------------------------
  -- * control shift delay
  -----------------------------------------------------------------------------
  process(clk)
  begin
    if rising_edge(clk) then
      lc_ena_r  <= ena;
      lc_rdy_ra <= lc_dv_s & lc_rdy_ra(0 to 1);
    end if;
  end process;

  lc_rdy_s <= lc_dv_s or lc_rdy_ra(0) or lc_rdy_ra(1) or lc_rdy_ra(2);
  lc_dv_s  <= lc_ena_r and not(ena);

  -----------------------------------------------------------------------------
  -- * apply algorithm
  -----------------------------------------------------------------------------
  ld_crc_s <= nextCRC32_D8(id, ld_crc_r);


  -----------------------------------------------------------------------------
  -- * data multiplexer
  -----------------------------------------------------------------------------
  lc_mux_s <= ena & lc_rdy_s;

  process(clk, lc_mux_s, ld_crc_s)
  begin
    if rising_edge(clk) then
      case lc_mux_s is
        when "00"   => ld_crc_r <= higha;
        when "01"   => ld_crc_r <= ld_crc_r(23 downto 0) & x"FF";
        when "10"   => ld_crc_r <= ld_crc_s;
        when "11"   => ld_crc_r <= higha;
        when others => null;
      end case;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- * output result
  -----------------------------------------------------------------------------

  od_crc <= ld_crc_r;

  odi : for i in 0 to 7 generate
    od(i) <= not(ld_crc_r(31-i));
  end generate odi;
  rdy <= lc_rdy_s;

end rtl;
