-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : dll_mac.vhd
-- Author     : Jose Correcher  <jocorso@jocor-virtualbox>
-- Company    :
-- Created    : 2020-04-27
-- Last update: 2020-05-27
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: Medium access Control. Functions performed in this subylayer:
--              - Receiver
--                * SFD recognition.
--                * MAC src/dst matching.
--                * Ethertype detection.
--                * CRC checking.
--              - Transceiver
--                * SFD append.
--                * MAC src/dst append.
--                * Ethertype append.
--                * CRC generate.
--              MAC broadcast covered.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-04-27  1.0      jocorso Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.record_declaration.all;
use work.constant_declaration.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity dll_mac is

  port (
    gbe_bus  : inout rc_gbe_bus;
    conf_bus : inout rc_conf_bus);

end entity dll_mac;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture rtl of dll_mac is

-- ** alias declaration

  alias rx_clk       : std_logic is gbe_bus.mii.rx_clk;
  alias id_phy       : unsigned is gbe_bus.mii.rx_data;
  alias ic_valid     : std_logic is gbe_bus.mii.rx_valid;
  alias od_mac_flags : unsigned is gbe_bus.dll.mac_flags;
  alias id_mac_dst   : unsigned is conf_bus.arp.mac_dst;

-- ** signal declaration
  signal b0_srl_ra        : unsigned(0 to C_SFD_END)       := (others => '0');
  signal b0_sfd_r         : unsigned(C_SFD_W-1 downto 0)   := (others => '0');
  signal b0_mask_s        : std_logic                      := '0';
  signal b0_data_r        : unsigned(id_phy'range)         := (others => '0');
  --
  signal b1_srl_ra        : unsigned(0 to C_MAC_END)       := (others => '0');
  signal b1_mac_r         : unsigned(2*C_MAC_W-1 downto 0) := (others => '0');
  signal b1_mask_s        : std_logic                      := '0';
  --
  signal b2_data_r        : unsigned(id_phy'range)         := (others => '0');
  signal b2_mask_r        : std_logic                      := '0';
  signal b2_soc_s         : std_logic                      := '0';
  signal b2_eof_s         : std_logic                      := '0';
  signal b2_crc_s         : unsigned(id_phy'range)         := (others => '0');
  signal b2_crc_residue_s : unsigned(C_CRC_RESIDUE'range)  := (others => '0');
  signal b2_rdy_s         : std_logic                      := '0';
  --
  signal b3_eof_r         : std_logic                      := '0';
  signal b3_sof_s         : std_logic                      := '0';
  signal b3_sfd_ok_s      : std_logic                      := '0';
  signal b3_mac_dst_ok_s  : std_logic                      := '0';
  signal b3_fcs_ok_s      : std_logic                      := '0';
  signal b3_flags_r       : unsigned(od_mac_flags'range)   := (others => '0');

begin

-- * RX

-------------------------------------------------------------------------------
-- ** B0 : Start Frame Delimiting
-------------------------------------------------------------------------------
--! This block will load the first 8 bytes corresponding to SFD:
--! 0x55 0x55 0x55 0x55 0x55 0x55 0x55 0xD5
--! A SFD enable mask will be created to filt SFD and check.

  process (rx_clk) is
  begin
    if rising_edge(rx_clk) then
      --! register data to synchronize with mask
      b0_data_r <= id_phy;
      --! b0_srl_ra shifts valid data to create SFD Mask.
      b0_srl_ra <= ic_valid & b0_srl_ra(0 to b0_srl_ra'length-2);
      --! b0_sfd_r stores SFD when valid, else cleans.
      if (ic_valid = '0') then
        b0_sfd_r <= (others => '0');
      else
        if (b0_mask_s = '1') then
          b0_sfd_r <= b0_sfd_r(b0_sfd_r'length-id_phy'length-1 downto 0) & b0_data_r;
        end if;
      end if;
    end if;
  end process;

  --! Creating SFD enable mask (8bytes)
  b0_mask_s <= b0_srl_ra(C_SFD_INI) and not b0_srl_ra(C_SFD_END);

-------------------------------------------------------------------------------
-- ** B1 : MAC
-------------------------------------------------------------------------------
--! This block load MAC source and destiny.
--! A MAC enable mask will be created to load MACs received in the current
--! message.

  process (rx_clk) is
  begin
    if rising_edge(rx_clk) then
      --! b0_srl_ra shifts valid data to create MAC mask.
      b1_srl_ra <= ic_valid & b1_srl_ra(0 to b1_srl_ra'length-2);
      --! b1_mac_r stores MACs when valid, else cleans.
      if (ic_valid = '0') then
        b1_mac_r <= (others => '0');
      else
        if (b1_mask_s = '1') then
          b1_mac_r <= b1_mac_r(b1_mac_r'length-id_phy'length-1 downto 0) & id_phy;
        end if;
      end if;
    end if;
  end process;

--! Creating MAC enable mask (2x6bytes)
  b1_mask_s <= b1_srl_ra(C_MAC_INI) and not b1_srl_ra(C_MAC_END);

-------------------------------------------------------------------------------
-- ** B2 : CRC
-------------------------------------------------------------------------------
--! This block will calculate FCS (CRC-32) residue. The calculation contains
--! MAC header + Ethertype + Payload + CRC.
--! A CRC enable mask will be created to enable CRC operation.
--! The result of the operation must be CRC-32 residue to catalog the frame
--! as valid.

  process (rx_clk) is
  begin
    if rising_edge(rx_clk) then
      --! when the frame ends, CRC enable deasserts
      if (b2_eof_s = '1') then
        b2_mask_r <= '0';
      else
        --! when SFD ends, CRC calculation mask is asserts
        if (b2_soc_s = '1') then
          b2_mask_r <= '1';
        end if;
      end if;
      --! shift register for input data
      b2_data_r <= id_phy;
    end if;
  end process;

  --! Start of CRC calculation
  b2_soc_s <= b0_srl_ra(C_SFD_END-1) and not b0_srl_ra(C_SFD_END);
  --! End of ethernet frame
  b2_eof_s <= b0_srl_ra(0) and not ic_valid;

  -- *** crc-32 instance
  u_rx_fcs : entity work.crc_32_ethernet
    port map (
      id     => b2_data_r,
      od     => b2_crc_s,
      od_crc => b2_crc_residue_s,
      ena    => b2_mask_r,
      rdy    => b2_rdy_s,
      clk    => rx_clk);

-------------------------------------------------------------------------------
-- ** B3 : Check RX
-------------------------------------------------------------------------------
--! This block checks all the earlier data loaded and generates enable masks in
--! order to send or filt data to the AXI bus.

  process (rx_clk) is
  begin
    if rising_edge(rx_clk) then
      --! shift register
      b3_eof_r <= b2_eof_s;
      --! when starts new frame all flags will cleaned, however check words and
      --! generate flags
      if (b3_sof_s = '1') then
        b3_flags_r <= (others => '0');
      else
        if (b3_sfd_ok_s = '1') then
          b3_flags_r(0) <= '1';
        end if;
        if (b3_mac_dst_ok_s = '1') then
          b3_flags_r(1) <= '1';
        end if;
        if (b3_fcs_ok_s = '1') then
          b3_flags_r(2) <= '1';
        end if;
      end if;
    end if;
  end process;

-- *** checking flags

  --! detect start frame delimiter -> 1: Ok, 0: Fail
  b3_sfd_ok_s     <= '1' when b0_sfd_r = C_PREAMBLE else '0';
  --! detect destiny mac -> 1: Ok, 0: Fail
  b3_mac_dst_ok_s <= '1' when b1_mac_r(2*C_MAC_W-1 downto C_MAC_W) = id_mac_dst
                     else '0';
  --! check FCS residue -> 1: Ok, 0: Fail
  b3_fcs_ok_s <= '1' when b2_crc_residue_s = C_CRC_RESIDUE else '0';

  --! detect start of new frame and clean flags
  b3_sof_s <= ic_valid and not b0_srl_ra(0);


  od_mac_flags <= b3_flags_r;

end architecture rtl;
