-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : gbe_data_link_layer.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-04-27
-- Last update: 2020-06-05
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This module contains all protocols grouped at Data Link Layer
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-04-27  1.0      jocorso	Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.record_declaration.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity gbe_data_link_layer is

  port (
    --!\name ethernet signals
    --!\{
    eth  : inout rc_eth_bus;
    --!\}
    --!\name control bus
    --!\{
    ctrl : inout rc_ctrl_bus
   --!\}
    );

end entity gbe_data_link_layer;


architecture structural of gbe_data_link_layer is

begin

-------------------------------------------------------------------------------
-- * B0 : MAC protocol
-------------------------------------------------------------------------------

  -- u_dll_mac : entity work.dll_mac
  --   port map (
  --     eth  => eth,
  --     ctrl => ctrl);

-------------------------------------------------------------------------------
-- * B1 : ARP protocol
-------------------------------------------------------------------------------

  u_dll_arp : entity work.dll_arp
    port map (
      eth  => eth,
      ctrl => ctrl);

end architecture structural;
