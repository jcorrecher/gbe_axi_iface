-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : gbe_media_independent_iface.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-04-30
-- Last update: 2020-06-05
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This module manages Media-Intermediate Interface.
--              GMII currently implemented. RGMII and SGMII for further designs
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-04-30  1.0      jocorso Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.record_declaration.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity gbe_media_independent_iface is

  port (
    --!\name signals from PHY
    --!\{
    phy : inout rc_gbe_phy_bus;
    --!\}
    --!\name internal bus for ethernet signals
    --!\{
    eth : inout rc_eth_bus
    --!\}
    );

end entity gbe_media_independent_iface;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture rtl of gbe_media_independent_iface is

-- ** constant declaration
  constant C_DLY : natural := 3;

-- ** type declaration
  type T_DATA is array (0 to C_DLY-1) of unsigned(phy.id_phy'range);

-- ** alias declaration
  alias rx_clk       : std_logic is phy.rx_clk;
  alias ic_phy_valid : std_logic is phy.ic_valid;
  alias id_phy       : unsigned  is phy.id_phy;
  alias od_phy       : unsigned  is phy.od_phy;
  alias oc_phy_valid : std_logic is phy.oc_valid;
  --
  alias clk          : std_logic is eth.mii.rx_clk;
  alias od_mii_data  : unsigned  is eth.mii.rx_data;
  alias oc_mii_valid : std_logic is eth.mii.rx_valid;
  alias ic_mii_valid : std_logic is eth.mii.tx_valid;
  alias id_mii_data  : unsigned  is eth.mii.tx_data;

-- ** signal declaration
  signal b0_data_ra  : T_DATA                 := (others => (others => '0'));
  signal b0_valid_ra : unsigned(0 to C_DLY-1) := (others => '0');

begin

-------------------------------------------------------------------------------
-- * port <-> signal assignment
-------------------------------------------------------------------------------

  od_mii_data  <= b0_data_ra(b0_data_ra'high);
  oc_mii_valid <= b0_valid_ra(b0_valid_ra'high);
  clk           <= rx_clk;

  od_phy       <= id_mii_data;
  oc_phy_valid <= ic_mii_valid;

-------------------------------------------------------------------------------
-- * B0 : GMII
-------------------------------------------------------------------------------
--! The data comes 8 bits per cycle (@125MHz) and all the design works
--! in that format, there's no need to adapt the data.

  process (rx_clk) is
  begin
    if rising_edge(rx_clk) then
      b0_data_ra  <= id_phy & b0_data_ra(0 to b0_data_ra'length-2);
      b0_valid_ra <= ic_phy_valid & b0_valid_ra(0 to b0_valid_ra'length-2);
    end if;
  end process;

end architecture rtl;
