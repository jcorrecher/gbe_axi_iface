-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : gbe_axi_iface.vhd
-- Author     : Jose Correcher  <jose.correcher@gmail.com>
-- Company    :
-- Created    : 2020-04-27
-- Last update: 2020-09-04
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This design establish an interface between gigabit Ethernet
--              PHY and AXI protocol.
--              This design will implement different ethernet layers.
--              - Media Independent Interface
--                * GMII
--              - Data Link Layer
--                * Media Access Control
--                * Adress Resolution Protocol
--              - Internet Layer
--                * Internet Protocol (v4)
--              - Network Layer
--                * User Datagram Protocol
--              AXI interface consist on:
--              - Control and status with AXI Stream interface.
--              - Data send and received by AXI Stream interface.
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-04-27  1.0      jocorso	Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.record_declaration.all;
use work.constant_declaration.all;

-------------------------------------------------------------------------------
-- * entity
-------------------------------------------------------------------------------

entity gbe_axi_iface is

  port (
    --!\name PHY bus
    --!\{
    phy : inout rc_gbe_phy_bus;
    --!\}
    --!\name AXI bus
    --!\{
    axi : inout rc_axi_bus
   --!\}
    );

end entity gbe_axi_iface;

-------------------------------------------------------------------------------
-- * architecture body
-------------------------------------------------------------------------------

architecture structural of gbe_axi_iface is

-- ** signal declaration
  signal ctrl : rc_ctrl_bus(conf(axi(data(C_CMD_W-1 downto 0)),
                                 dll(mac(mac_src(C_MAC_W-1 downto 0),
                                         mac_dst(C_MAC_W-1 downto 0)),
                                     arp(mac_src(C_MAC_W-1 downto 0),
                                         mac_dst(C_MAC_W-1 downto 0),
                                         ip_src(C_IP_W-1 downto 0),
                                         ip_dst(C_IP_W-1 downto 0),
                                         msg_type(C_BYTE_W-1 downto 0))
                                     )
                                 ),
                            stat(axi(data(C_CMD_W-1 downto 0)),
                                 dll(mac(rx_mac(C_MAC_W-1 downto 0)),
                                     arp(tx_mac(C_MAC_W-1 downto 0),
                                         tx_ip(C_IP_W-1 downto 0),
                                         tx_conf(C_BYTE_W-1 downto 0),
                                         rx_mac(C_MAC_W-1 downto 0),
                                         rx_ip(C_IP_W-1 downto 0),
                                         rx_conf(C_BYTE_W-1 downto 0))
                                     )
                                 )
                            );

  signal eth  : rc_eth_bus(mii(rx_data(C_BYTE_W-1 downto 0),
                               tx_data(C_BYTE_W-1 downto 0)),
                           dll(mac_flags(C_MAC_FLAGS_W-1 downto 0)));

begin

-------------------------------------------------------------------------------
-- * BC : AXI full configuration control
-------------------------------------------------------------------------------

  ctrl.clk <= eth.mii.rx_clk;

  u_gbe_ctrl : entity work.gbe_ctrl
    port map (
      ctrl => ctrl
      );

-------------------------------------------------------------------------------
-- * B0 : Media-independent interface
-------------------------------------------------------------------------------

  u_mii : entity work.gbe_media_independent_iface
    port map (
      phy => phy,
      eth => eth);

-------------------------------------------------------------------------------
-- * B1 : Data Link Layer
-------------------------------------------------------------------------------

  u_dll : entity work.gbe_data_link_layer
    port map (
      eth  => eth,
      ctrl => ctrl);

-------------------------------------------------------------------------------
-- * B2 : Internet Layer
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * B3 : Transport Layer
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * B4 : AXI stream interface
-------------------------------------------------------------------------------

  u_axi_iface : entity work.axi_iface
    port map (
      axi  => axi,
      ctrl => ctrl);

end architecture structural;
