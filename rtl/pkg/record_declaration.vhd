-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : record_declaration.vhd
-- Author     : Jose Correcher  <jocorso@jocor-virtualbox>
-- Company    :
-- Created    : 2020-04-27
-- Last update: 2020-06-08
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This file contains all record used in that design
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-04-27  1.0      jocorso Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * additional packages
-------------------------------------------------------------------------------

use work.constant_declaration.all;

-------------------------------------------------------------------------------
-- * package declaration
-------------------------------------------------------------------------------

package record_declaration is

-------------------------------------------------------------------------------
-- * PHY Interface bus
-------------------------------------------------------------------------------

  type rc_gbe_phy_bus is record
    rx_clk   : std_logic;
    ic_valid : std_logic;
    id_phy   : unsigned;
    tx_clk   : std_logic;
    oc_valid : std_logic;
    od_phy   : unsigned;
  end record rc_gbe_phy_bus;

-------------------------------------------------------------------------------
-- * AXI4 interface bus
-------------------------------------------------------------------------------

  -- ** AXI4 Stream
  type rc_axi_stream_bus is record
    aclk      : std_logic;
    arst      : std_logic;
    rx_tdata  : unsigned;
    rx_tvalid : std_logic;
    rx_tlast  : std_logic;
    rx_tkeep  : unsigned;
    rx_tready : std_logic;
    tx_tdata  : unsigned;
    tx_tvalid : std_logic;
    tx_tlast  : std_logic;
    tx_tkeep  : unsigned;
    tx_tready : std_logic;
  end record rc_axi_stream_bus;

  -- ** AXI4 Stream Lite
  type rc_axi_stream_lite_bus is record
    valid : std_logic;
    ready : std_logic;
    data  : unsigned;
  end record rc_axi_stream_lite_bus;

  -- ** AXI4 Control bus
  type rc_axi_ctrl_bus is record
    aclk : std_logic;
    arst : std_logic;
    cmd  : rc_axi_stream_lite_bus;
    stat : rc_axi_stream_lite_bus;
  end record rc_axi_ctrl_bus;

  -- ** top
  type rc_axi_bus is record
    ctrl : rc_axi_ctrl_bus;
--    data : rc_axi_stream_bus;
  end record rc_axi_bus;

-------------------------------------------------------------------------------
-- * Internal ethernet bus
-------------------------------------------------------------------------------

-- ** MII
  type rc_mii_bus is record
    rx_clk   : std_logic;
    rx_data  : unsigned;
    rx_valid : std_logic;
    tx_data  : unsigned;
    tx_valid : std_logic;
  end record rc_mii_bus;

-- ** DLL
  type rc_dll_bus is record
    mac_flags : unsigned;
  end record rc_dll_bus;

-- ** top
  type rc_eth_bus is record
    mii : rc_mii_bus;
    dll : rc_dll_bus;
  end record rc_eth_bus;

-------------------------------------------------------------------------------
-- * control bus
-------------------------------------------------------------------------------
--! Control bus drives all the signals related to control inside de design.
--! This bus is divided in 2 ways: configuration and status.
--! Each part contains sub busses for each layer and sublayer.
--! A hierarchical bus tree is described below.
--! ctrl
--!     | conf
--!           | axi
--!           | dll
--!                 \ arp
--!                 \ mac
--!     | stat
--!           | axi
--!           | dll
--!                 \ arp
--!                 \ mac

-- ***** mac configuration
  type rc_mac_cfg_bus is record
    mac_src : unsigned;
    mac_dst : unsigned;
    start   : std_logic;
    done    : std_logic;
  end record rc_mac_cfg_bus;

-- ***** arp configuration
  type rc_arp_cfg_bus is record
    mac_src  : unsigned;
    mac_dst  : unsigned;
    ip_src   : unsigned;
    ip_dst   : unsigned;
    msg_type : unsigned;
    start    : std_logic;
    done     : std_logic;
  end record rc_arp_cfg_bus;

-- **** data link layer confgiruation bus
  type rc_dll_cfg_bus is record
    mac : rc_mac_cfg_bus;
    arp : rc_arp_cfg_bus;
  end record rc_dll_cfg_bus;

-- *** configuration bus
  type rc_conf_bus is record
    axi : rc_axi_stream_lite_bus;
    dll : rc_dll_cfg_bus;
  end record rc_conf_bus;

-- ***** mac status
  type rc_mac_stat_bus is record
    rx_mac  : unsigned;
    rx_done : std_logic;
    tx_done : std_logic;
  end record rc_mac_stat_bus;

-- ***** arp status
  type rc_arp_stat_bus is record
    tx_mac  : unsigned;
    tx_ip   : unsigned;
    tx_conf : unsigned;
    tx_done : std_logic;
    rx_mac  : unsigned;
    rx_ip   : unsigned;
    rx_conf : unsigned;
    rx_done : std_logic;
  end record rc_arp_stat_bus;

-- -- **** data link layer status bus
  type rc_dll_stat_bus is record
    mac : rc_mac_stat_bus;
    arp : rc_arp_stat_bus;
  end record rc_dll_stat_bus;


-- *** status bus
  type rc_stat_bus is record
    axi : rc_axi_stream_lite_bus;
    dll : rc_dll_stat_bus;
  end record rc_stat_bus;


-- ** top
  type rc_ctrl_bus is record
    clk      : std_logic;
    arst     : std_logic;
    conf     : rc_conf_bus;
    stat     : rc_stat_bus;
  end record rc_ctrl_bus;

-------------------------------------------------------------------------------
-- to DEL
-------------------------------------------------------------------------------

-- ***** mac configuration
  -- type rc_mac_cfg_bus is record
  --   mac_src : unsigned;
  --   mac_dst : unsigned;
  --   start   : std_logic;
  --   done    : std_logic;
  -- end record rc_mac_cfg_bus;

-- ***** arp configuration
  -- type rc_arp_cfg_bus is record
  --   mac_src  : unsigned;
  --   -- mac_dst  : unsigned;
  --   -- ip_src   : unsigned;
  --   -- ip_dst   : unsigned;
  --   -- msg_type : unsigned;
  --   start    : std_logic;
  --   done     : std_logic;
  -- end record rc_arp_cfg_bus;

-- **** data link layer confgiruation bus
--   type rc_dll_cfg_bus is record
-- --    mac : rc_mac_cfg_bus;
--     arp : rc_arp_cfg_bus;
--   end record rc_dll_cfg_bus;

-- *** configuration bus
  -- type rc_conf_bus is record
  --   axi : rc_axi_stream_lite_bus;
  --   dll : rc_dll_cfg_bus;
  -- end record rc_conf_bus;

-- ***** mac status
  -- type rc_mac_stat_bus is record
  --   mac_rx  : unsigned;
  --   rx_done : std_logic;
  --   tx_done : std_logic;
  -- end record rc_mac_stat_bus;

-- ***** arp status
  -- type rc_arp_stat_bus is record
  --   tx_mac  : unsigned;
  --   tx_ip   : unsigned;
  --   tx_conf : unsigned;
  --   tx_done : std_logic;
  --   rx_mac  : unsigned;
  --   rx_ip   : unsigned;
  --   rx_conf : unsigned;
  --   rx_done : std_logic;
  -- end record rc_arp_stat_bus;

-- **** data link layer status bus
  -- type rc_dll_stat_bus is record
  --   mac : rc_mac_stat_bus;
  --   arp : rc_arp_stat_bus;
  -- end record rc_dll_stat_bus;

-- *** status bus
  -- type rc_stat_bus is record
  --   axi : rc_axi_stream_lite_bus;
  --   dll : rc_dll_stat_bus;
  -- end record rc_stat_bus;




end package record_declaration;
