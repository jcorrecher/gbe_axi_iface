-- * header
-------------------------------------------------------------------------------
-- Title      : Gigabit Ethernet - AXI Interface
-- Project    :
-------------------------------------------------------------------------------
-- File       : constant_declaration.vhd
-- Author     : Jose Correcher  <jocorso@jocor-virtualbox>
-- Company    :
-- Created    : 2020-04-27
-- Last update: 2021-03-17
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description: This package contains all constants used in that design
-------------------------------------------------------------------------------
-- Copyright (c) 2020
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2020-04-27  1.0      jocorso	Created
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- * libraries
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------
-- * package declaration
-------------------------------------------------------------------------------

package constant_declaration is

-------------------------------------------------------------------------------
-- * miscellaneous
-------------------------------------------------------------------------------

  -- ** size
  constant C_BYTE_W  : natural := 8;
  constant C_2BYTE_W : natural := 2 * C_BYTE_W;
  constant C_4BYTE_W : natural := 4 * C_BYTE_W;
  constant C_6BYTE_W : natural := 6 * C_BYTE_W;
  constant C_8BYTE_W : natural := 8 * C_BYTE_W;

-------------------------------------------------------------------------------
-- * AXI Interface
-------------------------------------------------------------------------------

  -- ** AXI-4 Lite
  constant C_S_AXI_ID_W     : natural := 16;
  constant C_S_AXI_DATA_W   : natural := 32;
  constant C_S_AXI_ADDR_W   : natural := 32;
  constant C_S_AXI_AWUSER_W : natural := 16;
  constant C_S_AXI_ARUSER_W : natural := 16;
  constant C_S_AXI_WUSER_W  : natural := 16;
  constant C_S_AXI_RUSER_W  : natural := 16;
  constant C_S_AXI_BUSER_W  : natural := 0;
  constant C_S_AXI_STRB_W   : natural := C_S_AXI_DATA_W / 8;
  constant C_AXI_DATA_W     : natural := 32;
  constant C_AXI_ADDR_W     : natural := 32;

-------------------------------------------------------------------------------
-- * data link layer
-------------------------------------------------------------------------------

  -- ** MAC

  -- *** start frame delimiter position
  constant C_SFD_INI  : natural := 0;
  constant C_SFD_END  : natural := 8;
  -- *** start frame delimiter constants
  constant C_SFD_W    : natural := 8 * C_BYTE_W;
  constant C_PREAMBLE : unsigned(C_SFD_W-1 downto 0) :=
    x"55_55_55_55_55_55_55_D5";
  -- *** mac position
  constant C_MAC_INI       : natural := 8;
  constant C_MAC_END       : natural := 20;
  -- *** mac constants
  constant C_MAC_W         : natural := 6 * C_BYTE_W;
  constant C_MAC_BROADCAST : unsigned(C_MAC_W-1 downto 0) :=
    x"FF_FF_FF_FF_FF_FF";
  constant C_MAC_EMPTY     : unsigned(C_MAC_W-1 downto 0) :=
    (others => '0');
  -- *** FCS constants
  constant C_CRC_RES_W   : natural := C_4BYTE_W;
  constant C_CRC_RESIDUE : unsigned(C_CRC_RES_W-1 downto 0) :=
    x"C7_04_DD_7B";
  -- *** mac flags
  constant C_MAC_FLAGS_W : natural := 4;

  -- ** ARP
  -- *** arp position
  constant C_ARP_INI   : natural := 28;
  constant C_ARP_END   : natural := 30;
  -- *** arp constants
  constant C_ARP_RQST  : unsigned(C_2BYTE_W-1 downto 0) := x"0100";
  constant C_ARP_RPLY  : unsigned(C_2BYTE_W-1 downto 0) := x"0200";
  constant C_HW_TYPE   : unsigned(C_2BYTE_W-1 downto 0) := x"0100";
  constant C_HW_SIZE   : unsigned(C_2BYTE_W-1 downto 0) := x"0406";
  constant C_PADDING   : unsigned(18*C_BYTE_W-1 downto 0) := (others => '0');
  constant C_ARP_MSG_W : natural := C_SFD_W + 2*C_MAC_W +
                                    5*C_2BYTE_W + 2*C_MAC_W +
                                    2*C_4BYTE_W + 18*C_BYTE_W;
  constant C_ARP_MSG_LEN : natural := (C_ARP_MSG_W / C_BYTE_W) - 1;

  -- ** type
  -- *** ethertype position
  constant C_ETH_INI   : natural := 20;
  constant C_ETH_END   : natural := 22;
  -- *** ethertype constants
  constant C_ETHERTYPE : unsigned(C_2BYTE_W-1 downto 0) := x"0608";

  constant C_IP_V4     : unsigned(C_2BYTE_W-1 downto 0) := x"0008";

-- * Internet layer
-- *** ip position
  constant C_IP0_INI : natural := 36;
  constant C_IP0_END : natural := 40;
  constant C_IP1_INI : natural := 46;
  constant C_IP1_END : natural := 50;
  constant C_IP_W    : natural := C_4BYTE_W;

-- *** IP constants
  constant C_IP_EMPTY : unsigned(C_IP_W-1 downto 0) := (others => '0');

-- * Transport Layer

-- *** UDP constants
constant C_UDP_W : natural := C_2BYTE_W;

-------------------------------------------------------------------------------
-- * command
-------------------------------------------------------------------------------

-- ** parameters by default
  constant C_UDP_DEFAULT : unsigned(C_UDP_W-1 downto 0) := x"11_20";
  constant C_IP_DEFAULT  : unsigned(C_IP_W-1 downto 0)  := x"AC_1E_A4_70";
  constant C_MAC_DEFAULT : unsigned(C_MAC_W-1 downto 0) := x"EE_FF_44_DB_55_00";


-- ** command word positions
  constant C_CMD_UDP_LSB : natural := 0;
  constant C_CMD_UDP_MSB : natural := C_CMD_UDP_LSB + C_UDP_W-1;
  constant C_CMD_IP_LSB  : natural := C_CMD_UDP_MSB + 1;
  constant C_CMD_IP_MSB  : natural := C_CMD_IP_LSB  + C_IP_W-1;
  constant C_CMD_MAC_LSB : natural := C_CMD_IP_MSB  + 1;
  constant C_CMD_MAC_MSB : natural := C_CMD_MAC_LSB + C_MAC_W-1;
  constant C_CMD_LSB     : natural := C_CMD_MAC_MSB + 1;
  constant C_CMD_MSB     : natural := C_CMD_LSB + C_BYTE_W-1;
-- ** command word width
  constant C_CMD_W       : natural := C_CMD_MSB + 1;
-- ** commands
  constant C_CMD_LOAD     : natural := 0;
  constant C_CMD_ARP_RQST : natural := 1;
  constant C_CMD_ARP_RPLY : natural := 2;


-------------------------------------------------------------------------------
-- * status
-------------------------------------------------------------------------------

-- ** command word positions
  constant C_STAT_UDP_LSB : natural := 0;
  constant C_STAT_UDP_MSB : natural := C_STAT_UDP_LSB + C_UDP_W-1;
  constant C_STAT_IP_LSB  : natural := C_STAT_UDP_MSB + 1;
  constant C_STAT_IP_MSB  : natural := C_STAT_IP_LSB  + C_IP_W-1;
  constant C_STAT_MAC_LSB : natural := C_STAT_IP_MSB  + 1;
  constant C_STAT_MAC_MSB : natural := C_STAT_MAC_LSB + C_MAC_W-1;
  constant C_STAT_LSB     : natural := C_STAT_MAC_MSB + 1;
  constant C_STAT_MSB     : natural := C_STAT_LSB + C_BYTE_W-1;

  constant C_STAT_ARP_RQST_TX : natural := 0;
  constant C_STAT_ARP_RQST_RX : natural := 1;
  constant C_STAT_ARP_RPLY_TX : natural := 2;
  constant C_STAT_ARP_RPLY_RX : natural := 3;
  constant C_STAT_MAC_TX      : natural := 4;
  constant C_STAT_MAC_RX      : natural := 5;
  constant C_STAT_IP_TX       : natural := 6;
  constant C_STAT_IP_RX       : natural := 7;
  constant C_STAT_UDP_TX      : natural := 8;
  constant C_STAT_UDP_RX      : natural := 9;

end package constant_declaration;
